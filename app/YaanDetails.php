<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class YaanDetails extends Model
{
    protected $table = 'yaan_details'; 
    protected $fillable = ['member_id','yaan_year','transaction_id'];
}
