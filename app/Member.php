<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = 'member_details'; 
    protected $fillable = ['current_location','country','dob_user','name','user_id','email','gender','college','branch','academicyear','year','mobile','alternate_mobile','other_country_input','reference','stripe_user'];
}




