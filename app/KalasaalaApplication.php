<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KalasaalaApplication extends Model
{
	protected $table = 'kalasaala_application'; 
	protected $fillable = ['art_form','age_category','member_id','transaction_id'];
}