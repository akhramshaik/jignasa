<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KalasaalaBatches extends Model {
	protected $table = 'kalasaala_batches'; 
	protected $fillable = ['type','category','date'];
}
