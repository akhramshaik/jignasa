<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Mail;
use Session;
use Payment;
use DB;

use App\Member;
use App\YaanDetails;
use App\KalasaalaApplication;

use Razorpay\Api\Api;


class PaymentController extends Controller
{


    public function paymentProcess(Request $request){
        $inputs = $request->all();
        $memberInfo = Member::where('member_id', $inputs['user_id'])->first();
        $transaction_id = strtoupper(str_random(12));

        $applicationData = ['art_form' => $inputs['art_cat'],'age_category' => $inputs['age_cat'],'member_id'=> $inputs['user_id'],'transaction_id'=> $transaction_id];
        $applicationInfo = KalasaalaApplication::create($applicationData);

        $paymentData = [
            'txnid' => $transaction_id,
            'amount' => 960,
            'productinfo' => "Jignasa Kalasaala",
            'firstname' => $memberInfo->name,
            'email' => $memberInfo->email,
            'phone' => $memberInfo->mobile,
            'lastname' => $memberInfo->member_id,
        ];

        return Payment::make($paymentData, function ($then) {
            $then->redirectRoute('paymentStatus');
        });
    }


    public function paymentProcessRazar(Request $request)
    {
        $input = $request->all();
        $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
        $payment = $api->payment->fetch($input['razorpay_payment_id']);
        if (count($input) && !empty($input['razorpay_payment_id'])) {
            try {
                $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount' => $payment['amount']));

            } catch (\Exception $e) {
                return redirect()->route('kalasaalaApplication');
            }
        }

        $transaction_id = strtoupper(str_random(12));

        if ($response) {
            $contactEmail = $response->email;
            $memberInfo = Member::where('email', $contactEmail)->first();
            $applicationData = ['art_form' => $input['art_cat'],'age_category' => $input['age_cat'],'member_id'=> $memberInfo->member_id,'transaction_id'=> $response->id];
            $applicationInfo = KalasaalaApplication::create($applicationData);

            $yaanDetails = ['member_id' => $memberInfo->member_id, 'yaan_year' => '2018', 'transaction_id' => $response->id];
            $yaanData = YaanDetails::insertGetId($yaanDetails);

            $paymentInfo = (object)(['firstname' => $memberInfo->name,'txnid' => $response->id, 'email' => $response->email, 'phone' => $response->contact]);

            // $data = array('paymentInfo' => $paymentInfo, 'yaanID' => $yaanData);
            // Mail::send('user.yaan_ticket', $data, function ($message) use ($contactEmail) {
            //     $message->to($contactEmail)->subject('Kalasaala - Payment confirmation');
            //     // $message->attach(public_path().'/documents/Jignasa_Yaan_2019_Consent_Letter.pdf');
            //     // $message->attach(public_path().'/documents/Jignasa_Yaan_2019_Disclaimer.pdf');
            //     // $message->attach(public_path().'/documents/Jignasa_Yaan_2019_Rules.pdf');
            //     $message->from('admin@jignasa.org', 'Jignasa');
            // });

            Session::flash('payment_message', 'Yay...! Your information is updated succesfully.');
            Session::flash('alert-class', 'alert-success');
            return view('user.payment_success')->with(['paymentInfo' => $paymentInfo, 'yaanID' => $yaanData]);
        } else {
            Session::flash('payment_message', 'Ohooo...! Something went wrong.');
            Session::flash('alert-class', 'alert-danger');
            return view('user.payment_failure')->with(['paymentInfo' => $paymentInfo]);
        }


    }

    public function paymentCheckout()
    {
        if ($userLogged = Sentinel::check()) {
            $memberInfo = Member::where('user_id', $userLogged->id)->first();
            $payCheck = DB::table('users')->where('id', $memberInfo->user_id)->first();
            if ($payCheck->last_name != 1) {
                DB::table('users')->where('id', $memberInfo->user_id)->update(['last_name' => 2]);
            }

            return view('user.payment_checkout')->with(['memberInfo' => $memberInfo]);
        }

        return redirect()->route('home');
    }


    public function paymentStatus()
    {

        $paymentStatusCheck = Session::get('tzsk_payu_data');

        if (empty($paymentStatusCheck)) {
            return redirect()->route('home');
        }


        $paymentInfo = Payment::capture();
        $paymentStatusFinal = $paymentInfo->isCaptured();

        if ($paymentStatusFinal) {
            DB::table('users')->where('email', $paymentInfo->email)->update(['last_name' => 1]);

            $contactEmail = $paymentInfo->email;
            $memberInfo = Member::where('email', $contactEmail)->first();

            $yaanDetails = ['member_id' => $memberInfo->member_id, 'yaan_year' => '2018', 'transaction_id' => $paymentInfo->txnid];
            $yaanData = YaanDetails::insertGetId($yaanDetails);


            // $data = array('paymentInfo' => $paymentInfo, 'yaanID' => $yaanData);
            // Mail::send('user.yaan_ticket', $data, function ($message) use ($contactEmail) {
            //     $message->to($contactEmail)->subject('Kalasaala - Payment confirmation');
            //     // $message->attach(public_path().'/documents/Jignasa_Yaan_2019_Consent_Letter.pdf');
            //     // $message->attach(public_path().'/documents/Jignasa_Yaan_2019_Disclaimer.pdf');
            //     // $message->attach(public_path().'/documents/Jignasa_Yaan_2019_Rules.pdf');
            //     $message->from('admin@jignasa.org', 'Jignasa');
            // });


            Session::flash('payment_message', 'Yay...! Your information is updated succesfully.');
            Session::flash('alert-class', 'alert-success');
            return view('user.payment_success')->with(['paymentInfo' => $paymentInfo, 'yaanID' => $yaanData]);
        } else {
            KalasaalaApplication::where('transaction_id',$paymentStatusCheck['data']['txnid'])->delete();
            Session::flash('payment_message', 'Ohooo...! Something went wrong.');
            Session::flash('alert-class', 'alert-danger');
            return view('user.payment_failure')->with(['paymentInfo' => $paymentInfo]);
        }


    }

    public function paymentStatus1()
    {

        return view('user.payment_failure');


    }


}
