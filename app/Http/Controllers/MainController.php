<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Mail;
use Session;
use DB;
use App\KalasaalaApplication;


// require_once(base_path() . '/vendor/stripe/stripe-php/init.php');
// \Stripe\Stripe::setApiKey(\getenv('STRIPE_SECRET'));
// \Stripe\Stripe::setClientId(\getenv('STRIPE_KEY'));

use App\Member;


class MainController extends Controller
{
    public function index()
    {
        return view('user.home');
    }


    public function privacyPolicy()
    {
        return view('user.privacy');
    }

    public function about()
    {
        return view('user.about');
    }

    public function events()
    {
        return view('user.events');
    }

    public function sac()
    {
        return view('user.sac');
    }

    public function collaborations()
    {
        return view('user.collaborations');
    }


    public function jignasayaan()
    {
        return view('user.jignasayaan');
    }
    
    public function bhuvanaVijayam()
    {
        return view('user.wak');
    }

    public function artathon()
    {
        return view('user.artathon');
    }

    public function amaravatiChandrudu()
    {
        return view('user.amaravatichandrudu');
    }

    public function mockParliament()
    {
        return view('user.mockparliament');
    }

    public function uniFest()
    {
        return view('user.unifest');
    }

    public function ekBharatShresthaBharat()
    {
        return view('user.ekbharatshresthabharat');
    }

    public function nIYC()
    {
        return view('user.niyc');
    }


    public function contact()
    {
        return view('user.contact');
    }

    public function faq()
    {
        return view('user.faq');
    }


    public function sponsers()
    {
        return view('user.sponsers');
    }

    public function login()
    {
        return view('user.login');
    }

    public function kalasaala()
    {
        return view('user.kalasaala');
    }


    public function register()
    {
        return view('user.register');
    }



    public function registrationsList()
    {
        $userInfo = Sentinel::check();

       $KalasaalaApplication = KalasaalaApplication::join('member_details', 'member_details.member_id', '=', 'kalasaala_application.member_id')
            ->get(['member_details.*', 'kalasaala_application.*']);


        $transaction_id = strtoupper(str_random(12));

        return view('user.all-registrations')->with(['KalasaalaApplication' => $KalasaalaApplication,'transaction_id' => $transaction_id]);
    }

    public function kalasaalaApplication()
    {
        $userInfo = Sentinel::check();

        $KalasaalaApplication = KalasaalaApplication::where('member_id', $userInfo->id)->get();
        $transaction_id = strtoupper(str_random(12));

        return view('user.kalasaala_application')->with(['KalasaalaApplication' => $KalasaalaApplication,'transaction_id' => $transaction_id]);
    }

    public function kalasaalaNewApplication(){
		$userInfo = Sentinel::check();
		$memberInfo = Member::where('user_id', $userInfo->id)->first();
		return view('user.kalasaala_new_application')->with(['memberInfo' => $memberInfo]);
	}


    public function loginProcess(Request $request)
    {
        $inputs = $request->all();
        // print_r($inputs);

        $credentials = [
            'email' => $inputs['email'],
            'password' => $inputs['password'],
        ];

        $userCheck = Sentinel::authenticateAndRemember($credentials);

        if ($userCheck) {
            return redirect()->route('home');
        }

        Session::flash('chk_msg', 'Ohooo...! You tried to login with Invalid Credentials.');
        Session::flash('alert-class', 'alert-danger');
        return redirect()->route('login');


    }

    public function logout()
    {
        $userLogout = Sentinel::logout();
        if ($userLogout) {
            return redirect()->route('home');
        }

    }


    public function myaccount()
    {
        $userAccount = Sentinel::check();
        $memberData = Member::where('user_id', $userAccount->id)->first();


        if ($userAccount) {
            return view('user.my_account')->with(['userAccount' => $userAccount, 'memberData' => $memberData]);
        }

        return redirect()->route('home');
    }


    public function myhistory()
    {
        $userAccount = Sentinel::check();
        $memberData = Member::where('user_id', $userAccount->id)->first();
        $paymentDetails = DB::table('payu_payments')->where('email', $memberData->email)->where('status', 'Completed')->get();
        if ($userAccount) {
            return view('user.my_history')->with(['paymentDetails' => $paymentDetails, 'userAccount' => $userAccount, 'memberData' => $memberData]);
        }

        return redirect()->route('home');
    }


    public function editProcess(Request $request)
    {
        $inputs = $request->all();


        $memberData = ['name' => $inputs['member_name'],
            'email' => $inputs['member_email'],
            'gender' => $inputs['member_gender'],
            'college' => $inputs['college_name'],
            'current_location' => $inputs['current_location'],
            'country' => $inputs['country'],
            'dob_user' => $inputs['dob_user'],
            'reference' => $inputs['reference'],
            'other_country_input' => $inputs['other_country'],
            'branch' => '',
            'academicyear' => '',
            'year' => '',
            'mobile' => $inputs['contact_number'],
            'alternate_mobile' => $inputs['emrgency_number'],
        ];


        $memberUpdate = Member::where('user_id', $inputs['user_id'])->update($memberData);

        if ($memberUpdate) {
            Session::flash('chk_msg', 'Yay...! Your information is updated succesfully.');
            Session::flash('alert-class', 'alert-success');
        } else {
            Session::flash('chk_msg', 'Ohooo...! Something went wrong.');
            Session::flash('alert-class', 'alert-danger');
        }

        return redirect()->route('myaccount');
    }

    public function passwordProcess(Request $request)
    {

    }


    public function registerProcess(Request $request)
    {
        $inputs = $request->all();

        $credentialsCheck = [
            'login' => $inputs['email']
        ];

        $userCheck = Sentinel::findByCredentials($credentialsCheck);

        if ($userCheck) {
            Session::flash('chk_msg', 'Sorry...! This email is already registered with Us.');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('register');
        }


        $credentials = [
            'email' => $inputs['email'],
            'password' => $inputs['password'],
            'first_name' => $inputs['name'],
        ];
        $userRegister = Sentinel::registerAndActivate($credentials);

        if ($userRegister) {
            $memberData = ['name' => $inputs['name'],
                'email' => $inputs['email'],
                'user_id' => $userRegister->id,
                'gender' => '',
                'college' => '',
                'current_location' => '',
                'country' => '',
                'dob_user' => '',
                'reference' => '',
                'other_country_input' => '',
                'branch' => '',
                'academicyear' => '',
                'year' => '',
                'mobile' => $inputs['mobile'],
                'alternate_mobile' => ''
            ];
            $memberInsert = Member::create($memberData);

            Sentinel::authenticateAndRemember($credentials);


            if ($memberInsert) {
                Session::flash('chk_msg', 'Yay...! Your account was created succesfully. Please update required information below.');
                Session::flash('alert-class', 'alert-success');
                $userAccount = Sentinel::check();
                $memberData = Member::where('user_id', $userAccount->id)->first();
                return redirect()->route('myaccount')->with(['userAccount' => $userAccount, 'memberData' => $memberData]);
            }


        }


        // $data = array('name'=>"Virat Gandhi");
        // Mail::send('user.activation_mail', $data, function($message) {
        //    $message->to('akhramshaik@gmail.com', 'Tutorials Point')->subject('Please confirm your account.');
        //    $message->from('admin@jignasa.org','Jignasa');
        // });


    }

}
