<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Sentinel;
use Auth;
use Socialite;

use App\Member;



class SocialLoginController extends Controller
{

    public function fbLogin()
    {
        return Socialite::driver('facebook')->redirect();   
    }   


    public function gmailLogin()
    {
        return Socialite::driver('google')->redirect();   
    }   





    public function gmailCallback(Request $request)
    {
   
        $providerUser = Socialite::driver('google')->user(); 

        $credentialsCheck = [
            'login' => $providerUser->email,
        ];

        $credentialsFull = [
            'email'    => $providerUser->email,
            'password' => $providerUser->id,
            'first_name' => $providerUser->name,
        ];

        $userCheck = Sentinel::findByCredentials($credentialsCheck);

        if($userCheck){
          $userLogin = $userCheck;
          Sentinel::authenticateAndRemember($userLogin);
          return redirect()->to('/home');
        } else{
		        $userRegister = Sentinel::registerAndActivate($credentialsFull);
		        $userLogin = $userRegister;

				if($userRegister){
					$memberData = ['name' => $providerUser->getName(),
					               'email' => $providerUser->getEmail(),
					               'user_id' => $userRegister->id,
					               'gender' => '',
					               'college' => '',
					               'branch' => '',
					               'academicyear' => '',
					               'year' => '',
					               'mobile' => '',
					               'alternate_mobile' => ''
					           ];
			        $memberInsert = Member::create($memberData);
			        Sentinel::authenticateAndRemember($userLogin);

				if($memberInsert){
					Session::flash('chk_msg', 'Yay...! Your account was created succesfully. Please update required information below.'); 
		            Session::flash('alert-class', 'alert-success'); 
			    	$userAccount = Sentinel::check();
		     		$memberData = Member::where('user_id',$userAccount->id)->first();
					return redirect()->route('myaccount')->with(['userAccount' => $userAccount, 'memberData' => $memberData]);
				}
        }
        }

        Sentinel::authenticateAndRemember($userLogin);
        return redirect()->to('/home');
    }


    public function fbCallback(Request $request)
    {
   
        $providerUser = Socialite::driver('facebook')->user(); 
        $credentialsCheck = [
            'login' => $providerUser->getEmail(),
        ];

        $credentialsFull = [
            'email'    => $providerUser->getEmail(),
            'password' => $providerUser->getId(),
            'first_name' => $providerUser->getName(),
        ];


        $userCheck = Sentinel::findByCredentials($credentialsCheck);


        if($userCheck){
          $userLogin = $userCheck;
          Sentinel::authenticateAndRemember($userLogin);
          return redirect()->to('/home');
        } else{

		        $userRegister = Sentinel::registerAndActivate($credentialsFull);
		        $userLogin = $userRegister;
		        
				if($userRegister){
					$memberData = ['name' => $providerUser->getName(),
					               'email' => $providerUser->getEmail(),
					               'user_id' => $userRegister->id,
					               'gender' => '',
					               'college' => '',
					               'branch' => '',
					               'academicyear' => '',
					               'year' => '',
					               'mobile' => '',
					               'alternate_mobile' => ''
					           ];
			        $memberInsert = Member::create($memberData);
			        Sentinel::authenticateAndRemember($userLogin);


				if($memberInsert){
					Session::flash('chk_msg', 'Yay...! Your account was created succesfully. Please update required information below.'); 
		            Session::flash('alert-class', 'alert-success'); 
			    	$userAccount = Sentinel::check();
		     		$memberData = Member::where('user_id',$userAccount->id)->first();
					return redirect()->route('myaccount')->with(['userAccount' => $userAccount, 'memberData' => $memberData]);
				}
        }
    }
    }






}
