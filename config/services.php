<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    // 'facebook' => [
    //     'client_id' => '1797290933680500',
    //     'client_secret' => 'f1780364e4bb4e1380d92f0a87b15ca6',
    //     'redirect' => 'https://jignasa.co/fb-process-response',
    // ],

    'facebook' => [
        'client_id' => '2124054374582277',
        'client_secret' => '20142753c6a8da1b1be4da64652f65b4',
        'redirect' => 'https://jignasa.org/fb-process-response',
    ],

    // 'facebook' => [
    //     'client_id' => '240725743313374',
    //     'client_secret' => '2dc817a5061fcaa5f2da34475c10a792',
    //     'redirect' => 'https://devmobtest.co/fb-process-response',
    // ],

    // 'google' => [
    //     'client_id' => '277883038205-up1ls9fl38r92tfrgan00vuc69cdrn4f.apps.googleusercontent.com',
    //     'client_secret' => 'hIIEPZ2caqlwlfIPYmtE02Xw',
    //     'redirect' => 'https://jignasa.co/gmail-process-response',
    // ],  

    'google' => [
        'client_id' => '277883038205-tb2bk2d3b6t6u83bskcgpp7iv37inj7q.apps.googleusercontent.com',
        'client_secret' => '1YSiQURAhHpDAFjrEa0H9z7U',
        'redirect' => 'https://jignasa.org/gmail-process-response',
    ],  


    // 'google' => [
    //     'client_id' => '65529592331-i05o558dlgtdri0nb4c7u7ui6pn52d2a.apps.googleusercontent.com',
    //     'client_secret' => 'PDTonIq6Ag2DcaQpNYO1xemB',
    //     'redirect' => 'https://devmobtest.co/gmail-process-response',
    // ],
    

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

];

