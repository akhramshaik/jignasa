<?php

return [

    'env' => 'secure',
    // 'env' => 'test',

    'default' => 'payumoney',

    'accounts' => [
        'payumoney' => [
            'key' => '0cDIHrlO',
            'salt' => 'zt7pPvFjVu',
            'money' => true,
            'auth' => '7NXG7caLjIe55x6676BhBg4nkJdzIs88e367Hf+QLoA='
        ],
    ],


// Test
    // 'accounts' => [
    //     'payumoney' => [
    //         'key' => '0cDIHrlO',
    //         'salt' => 'zt7pPvFjVu',
    //         'money' => true,
    //         'auth' => '7NXG7caLjIe55x6676BhBg4nkJdzIs88e367Hf+QLoA='
    //     ],
    // ],

    'endpoint' => 'payu.in/_payment',

    'driver' => 'database',

    'table' => 'payu_payments',
];
