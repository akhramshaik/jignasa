<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Auth::routes();

Route::get('/', 'MainController@index')->name('home');
Route::get('/home', 'MainController@index')->name('home');
Route::get('/about-us', 'MainController@about')->name('about');
Route::get('/events', 'MainController@events')->name('events');
Route::get('/contact-us', 'MainController@contact')->name('contact');
Route::get('/frequently-asked-questions', 'MainController@faq')->name('faq');
Route::get('/jignasa-sponsers', 'MainController@sponsers')->name('sponsers');


Route::get('/jignasa-collaborations', 'MainController@collaborations')->name('collaborations');


Route::get('/kalasaala', 'MainController@kalasaala')->name('kalasaala');

Route::get('/login', 'MainController@login')->name('login');
Route::post('/loginProcess', 'MainController@loginProcess')->name('loginProcess');
Route::get('/register', 'MainController@register')->name('register');
Route::get('/logout', 'MainController@logout')->name('logout');
Route::get('/my-account', 'MainController@myaccount')->name('myaccount');
Route::get('/my-history', 'MainController@myhistory')->name('myhistory');
Route::post('/registerProcess', 'MainController@registerProcess')->name('registerProcess');
Route::post('/editProcess', 'MainController@editProcess')->name('editProcess');
Route::post('/passwordProcess', 'MainController@passwordProcess')->name('passwordProcess');

Route::get('/privacy-policy', 'MainController@privacyPolicy')->name('privacyPolicy');
Route::get('/kalasaala-applications', 'MainController@kalasaalaApplication')->name('kalasaalaApplication');

Route::get('/registrations-list', 'MainController@registrationsList')->name('registrationsList');

Route::get('/kalasaala-application', 'MainController@kalasaalaNewApplication')->name('kalasaalaNewApplication');


Route::get('/fb-login', 'SocialLoginController@fbLogin')->name('fbLogin');
Route::get('/fb-process-response', 'SocialLoginController@fbCallback');

Route::get('/gmail-login', 'SocialLoginController@gmailLogin')->name('gmailLogin');
Route::get('/gmail-process-response', 'SocialLoginController@gmailCallback');


Route::get('/student-activity-centres', 'MainController@sac')->name('sac');
Route::get('/jignasa-yaan', 'MainController@jignasayaan')->name('jignasayaan');
Route::get('/jignasa-yaan-2018', 'MainController@jignasayaan');
Route::get('/bhuvana-vijayam', 'MainController@bhuvanaVijayam')->name('bhuvana-vijayam');
Route::get('/artathon', 'MainController@artathon')->name('artathon');
Route::get('/amaravati-chandrudu', 'MainController@amaravatiChandrudu')->name('amaravatiChandrudu');
Route::get('/mock-parliament', 'MainController@mockParliament')->name('mockParliament');
Route::get('/unifest', 'MainController@uniFest')->name('uniFest');
Route::get('/ek-bharat-shrestha-bharat', 'MainController@ekBharatShresthaBharat')->name('ekBharatShresthaBharat');
Route::get('/NIYC', 'MainController@nIYC')->name('nIYC');

Route::get('/payment-status1', 'PaymentController@paymentStatus1')->name('paymentStatus1');
Route::get('/payment-checkout', 'PaymentController@paymentCheckout')->name('paymentCheckout');
Route::get('/payment-status', 'PaymentController@paymentStatus')->name('paymentStatus');
Route::post('/payment-processing', 'PaymentController@paymentProcess')->name('paymentProcess');
Route::post('/paymentProcessRazar', 'PaymentController@paymentProcessRazar')->name('paymentProcessRazar');

Route::get('/fb-login', 'SocialLoginController@fbLogin')->name('fbLogin');