@extends('user.master')
@section('content')
@include('user.master_header')
<main class="xs-main" style="margin-top: 150px;">
   <section class="xs-content-section-padding">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="xs-event-banner">
                  <img src="assets/images/event/event-banner.jpg" alt="">
               </div>
               <div class="row">
                  <div class="col-lg-12 xs-event-wraper" style="padding: 15px 0px;">
                     <center>
                        @if(Sentinel::getUser())
                        <a href="{{ route('paymentCheckout') }}"><button type="button" class="btn btn-success">Register Now</button></a>
                        @else
                        <a href="{{ route('login') }}"><button type="button" class="btn btn-success">Register Now</button></a>
                        @endif
                     </center>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-12 xs-event-wraper">
                     <div class="xs-event-content">
                        <h4>Jignasa Yaan 2019</h4>
                        <p class="lead">JIGNASA YAAN is a Knowledge Journey by professional students of local colleges from states of Andhra Pradesh, Telangana, Tamil Nadu to IIT and IIM Campus Festivals helping the students to experience the Knowledge and Creativity through Cross Culture student interaction with students and student organizers of IITs, IIM’s, NIT’s, BITS from different states and corners of our country.</p>
                        <p class="lead">JIGNASA YAAN is started with a motto to help the students experience the broad world in the domains of Knowledge, Literature, Culture, Arts, Heritage, Entrepreneurship, Leadership and Life Skill Development through Knowledge Journeys & Cross Culture Interaction initiatives to Campus Festivals of Elite Institutions of the country. </p>
                        <p class="lead" style="color: #ff853f;">JIGNASA YAAN was started in January 2014 to IIT Madras with 120 students from different colleges.</p>
                     </div>


                     <div class="xs-about-feature xs-mb-30">
                        <h3>Our Vision</h3>
                        <p class="lead" style="margin-bottom: 20px;">JIGNASA YAAN On travelling world with a specific purpose, meeting different kinds of people from different cultures experiencing different climatic, geographical, historical and societal situations, the students shall get a valuable experience and glimpse of world and people. With such knowledge filled glimpse students will be ready to lead his professional life with wisdom and vision, live life with satisfaction and happiness in different situations and circumstances the life presents.</p>
                        <p class="lead" style="margin-bottom: 20px;">To revive such wonderful vision and joy filled experience of learning, JIGNASA is conducting Modern DesaYatana with the name JIGNASA YAAN helping the students to experience Activity and Interaction based Learning and Knowledge. The student participants of JIGNASA YAAN termed as YAANIKs shall experience activity based learning with creative and inspiration filled experience of places and people of Indian Arts, Villages, Culture, Heritage, Environment and Social responsibility as a part of their Knowledge Journey.</p>
                     </div>

                                    <div class="row">
                  <div class="col-lg-12 xs-event-wraper" style="padding: 15px 0px;">
                     <center>
                        @if(Sentinel::getUser())
                        <a href="{{ route('paymentCheckout') }}"><button type="button" class="btn btn-success">Register Now</button></a>
                        @else
                        <a href="{{ route('login') }}"><button type="button" class="btn btn-success">Register Now</button></a>
                        @endif
                     </center>
                  </div>
               </div>
                     <div class="xs-horizontal-tabs">
                        <ul class="nav nav-tabs" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" href="#facilities" role="tab">Our History</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#mapLocation" role="tab">Location</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#contactUs" role="tab">Contact us</a>
                           </li>
                        </ul>
                        <div class="tab-content">
                           <div class="tab-pane fade show active" id="facilities" role="tabpanel">
                              <p>Till now we have conducted Five Jignasa Yaan’s.</p>
                              <div class="row">
                                 <div class="col-md-12">
                                    <ul class="xs-unorder-list circle green-icon">
                                       <li>Jignasa Yaan 1 with 120 students to IIT Madras, Tamil Nadu State on January 2014. </li>
                                       <li>Jignasa Yaan 2 with 160 students to BITS Pilani, Rajasthan State on October 2014. </li>
                                       <li>Jignasa Yaan 3 with 110 students to IIT BHU, Uttar Pradesh State on February 2015. </li>
                                       <li>Jignasa Yaan 4 with 436 students to IIST Trivandrum, Kerala State on October 2015. </li>
                                       <li>Jignasa Yaan 5 with 450 students to IIM Ahemdabad, Gujarat State on February 2016. </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <div class="tab-pane" id="mapLocation" role="tabpanel">
                              <div id="xs-map"></div>
                           </div>
                           <div class="tab-pane" id="contactUs" role="tabpanel">
                              <div class="xs-contact-form-wraper">
                                 <form action="#" method="POST" id="xs-contact-form" class="xs-contact-form">
                                    <div class="input-group">
                                       <input type="text" name="name" id="xs-name" class="form-control" placeholder="Enter Your Name.....">
                                       <div class="input-group-append">
                                          <div class="input-group-text"><i class="fa fa-user"></i></div>
                                       </div>
                                    </div>
                                    <div class="input-group">
                                       <input type="email" name="email" id="xs-email" class="form-control" placeholder="Enter Your Email.....">
                                       <div class="input-group-append">
                                          <div class="input-group-text"><i class="fa fa-envelope-o"></i></div>
                                       </div>
                                    </div>
                                    <div class="input-group massage-group">
                                       <textarea name="massage" placeholder="Enter Your Message....." id="xs-massage" class="form-control" cols="30" rows="10"></textarea>
                                       <div class="input-group-append">
                                          <div class="input-group-text"><i class="fa fa-pencil"></i></div>
                                       </div>
                                    </div>
                                    <button class="btn btn-success disabled" disabled="" type="submit" id="xs-submit">submit</button>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</main>
@endsection