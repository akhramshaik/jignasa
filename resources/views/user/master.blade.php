<!doctype html>
<html class="no-js" lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>Jignasa - Inquire Initiate Inspire... Intellect</title>
      <meta name="description" content="Jignasa started with a motto to promote Entrepreneurship in Culture, Fine Arts, Heritage, Literature through promoting Life Skill Development and Creative Leadership with Education initiatives and activities.">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      
      <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CRoboto+Slab:400,700" rel="stylesheet">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">

      <link rel="icon" type="image/png" href="assets/images/favicon.ico">
      <link rel="apple-touch-icon" href="apple-touch-icon.png">
      <link rel="stylesheet" href="{{ URL::asset('user/css/font-awesome.min.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('user/css/bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('user/css/xsIcon.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('user/css/isotope.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('user/css/magnific-popup.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('user/css/owl.carousel.min.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('user/css/owl.theme.default.min.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('user/css/animate.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('user/css/plugins.css') }}" />
      <link rel="stylesheet" href="{{ URL::asset('user/css/style.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('user/css/responsive.css') }}" />
      <link rel='stylesheet' type='text/css' href="{{ URL::asset('user/css/colors/color-1.css') }}" >
      
      <script src="{{ URL::asset('user/js/jquery-3.2.1.min.js') }}"></script>
      <script src="{{ URL::asset('user/js/bootstrap.min.js') }}"></script>
      <script src="{{ URL::asset('user/js/main.js') }}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
   </head>
   <body>
   <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
   <![endif]-->

   <div id="preloader">
      <div class="spinner">
         <div class="double-bounce1"></div>
         <div class="double-bounce2"></div>
      </div>
   </div>

               @include('user.master_header')


               @yield('content')


               @include('user.master_footer')






      <script src="{{ URL::asset('user/js/plugins.js') }}"></script>
      <script src="{{ URL::asset('user/js/isotope.pkgd.min.js') }}"></script>
      <script src="{{ URL::asset('user/js/jquery.magnific-popup.min.js') }}"></script>
      <script src="{{ URL::asset('user/js/owl.carousel.min.js') }}"></script>
      <script src="{{ URL::asset('user/js/jquery.waypoints.min.js') }}"></script>
      <script src="{{ URL::asset('user/js/jquery.countdown.min.js') }}"></script>
      <script src="{{ URL::asset('user/js/spectragram.min.js') }}"></script>


<script type="text/javascript">
$(document).ready(function() {
    $('#js-date').datepicker({autoclose: true,format: 'mm-dd-yyyy',});
});

</script>



   </body>
</html>



