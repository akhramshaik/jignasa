@extends('user.master')
@section('content')
    @include('user.master_header')
    <main class="xs-main" style="margin-top: 130px;">
        <section class="xs-section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="xs-text-content xs-pr-20">
                            <h2 class="color-navy-blue">Interested in Sponsorship ? </h2>
                            <p>JIGNASA is a medium to reach thousands of students, Best place to promote brands and new
                                gadgets and to be a part of Arts, Culture & Heritage Entrepreneur Revolution.</p>
                            <blockquote>
                                For any further queries regarding Sponsorship. Call us at +91-97003 30899 or drop us a mail at hr@jignasa.org
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection