@extends('user.master')
@section('content')
@include('user.master_header')

<style type="text/css">
   p {
          text-align: justify;
   }
</style>
<main class="xs-main" style="margin-top: 150px;">
   <section class="xs-content-section-padding">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="xs-event-banner">
                  <img src="assets/images/waclogo.jpeg" alt="">
               </div>

               <div class="row">
                            <div class="col-lg-12 xs-event-wraper" style="padding: 15px 0px;">
                                <center>
                                                                            <a target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSdyDuWLAu4tPOrDkb2qZGWkx9QODF6cyBWz2bbH20COBmkdQw/viewform">
                                            <button type="button" class="btn btn-success">Register Now</button>
                                        </a>
                                                                    </center>
                            </div>
                        </div>


               <div class="row">
                  <div class="col-lg-12 xs-event-wraper">
                     <div class="xs-event-content">
                        <h4>WAK - Wisdom And Knowledge (Bhuvana Vijayam)</h4>
                        <p class="lead">The court of the Emperor Sri Krishnadevaraya, consisting of eight great scholars
and poets, the Ashtadiggajas, is also called as <b>Bhuvana Vijayam</b>, which translates
to conquest of the World. Ever wondered why it is called the Conquest of the
World? Because it does feel like you are winning the world when you are
completely involved, with all of your mind, body and soul, in scholarly discourses
with highly talented and elite artistic people. We humans are highly social
beings. We crave to be connected with other people. We crave to connect with
people who match our intellectual level and who challenge our intellectual
capabilities. You feel joy in art, not when you win the competition but while you
are being challenged by the competitive opponent to push yourself to the limits
of your artistic capability, the place where you find the real you. It is then and
there you explore your own abilities and other dimensions of your own self. That
is when you feel like you are winning the world. Bhuvana Vijayam. To create such
an opportunity, Jignasa Interface launched WAK WAC, a platform that brings
together artists from different cultural backgrounds under the grounds of
common talents, interests and vision.</p>

                     </div>

                     <div class="xs-event-content">
                        <h3>What is Jignasa ?</h3>
                        <p class="lead">JIGNASA is an enterprise started with a motto to promote and execute the vision
of first Indian Nobel laureate Sri Ravindranath Tagore who aptly said "Education
is that which not only includes mere academics, but also Fine Arts, Culture,
Heritage, Social Responsibility as a part of it for over all development of the
individual". It strives to build a sustainable ecosystem pertaining to arts, culture
and heritage. JIGNASA is the largest professional student network in Andhra
Pradesh and Telangana with around 800+ organisers, 3500+ volunteers , 1000+
artists impacting over 2,60,000+ students and induviduals directly through
2000+ educative initiatives and activities across 25 states and over 8 years.
</p>

                     </div>
                           <div class="xs-event-content">
                        <h3>What is Bhuvana Vijayam ?</h3>
                        <p class="lead">Bhuvana Vijayam is a highly
engagement-oriented platform designed
for artists from different fields of Dance,
Music, Drama, Literature and Arts to join for
an art-party every weekend where they
explore, present and improve their skills by
collaborating with co-participants while
getting mentored by experts in respective
fields. This is a not a competition but a
collaboration to experience bliss through
meaningful engagement with fellow artists
</p>

                     </div>

                           <div class="xs-event-content">
                        <h3>Bhuvana Vijayam (WAK - Wisdom and Knowledge ) </h3>
               <div class="xs-event-banner" style="text-align: center">
                  <img src="assets/images/bhuvana_act.png" alt="">
               </div>

                     </div>


                  </div>

               </div>
            </div>
         </div>
      </div>
   </section>
</main>
@endsection