@extends('user.master')
@section('content')
    @include('user.master_header')
    <main class="xs-main" style="margin-top: 130px;">
        <div class="xs-funfact-section-v2 waypoint-tigger"
             style="background-image: url('assets/images/map_green.png')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="xs-single-funFact funFact-v2">
                                <span class="number-percentage-count number-percentage" data-value="25"
                                      data-animation-duration="3500">25</span><span>+</span>
                            <small>Colleges</small>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="xs-single-funFact funFact-v2">
                                <span class="number-percentage-count number-percentage" data-value="250000"
                                      data-animation-duration="3500">250,000</span><span>+</span>
                            <small>Students</small>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="xs-single-funFact funFact-v2">
                                <span class="number-percentage-count number-percentage" data-value="2500"
                                      data-animation-duration="3500">2500</span><span>+</span>
                            <small>Events</small>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="xs-single-funFact funFact-v2">
                                <span class="number-percentage-count number-percentage" data-value="50"
                                      data-animation-duration="3500">50</span><span>+</span>
                            <small>Associations</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="xs-section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="xs-text-content xs-pr-20">
                            <h2 class="color-navy-blue">Our Partners</h2>
                            <p>JIGNASA is a medium to reach out thousands of students, Best place to promote brands and new
                                gadgets and to be a part of Arts, Culture & Heritage Entrepreneur Revolution.</p>
                        </div>
                    </div>
                    <div class="col-lg-2"><img src="assets/images/partner/1.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/2.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/3.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/4.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/5.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/6.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/7.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/8.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/9.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/11.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/12.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/13.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/14.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/15.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/16.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/17.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/18.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/19.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/20.png" alt=""></div>
                    <div class="col-lg-2"><img src="assets/images/partner/21.png" alt=""></div>

                </div>
            </div>
        </section>

        <section class="xs-section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="xs-text-content xs-pr-20">
                            <h2 class="color-navy-blue">Interested to Collaborate with Us? </h2>
                            <p>JIGNASA is a medium to reach thousands of students, Best place to promote brands and new
                                gadgets and to be a part of Arts, Culture & Heritage Entrepreneur Revolution.</p>
                            <blockquote>
                                For any further queries regarding Collaborations. Call us at +91-97003 30899 or drop us
                                a mail at hr@jignasa.org
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection