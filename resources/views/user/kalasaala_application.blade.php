@extends('user.master')
@section('content')
    @include('user.master_header')
    <main class="xs-main" style="margin-top: 150px;">
        <section class="xs-section-padding" style="padding: 10px 0;">
            <div class="row">
                <div class="col-md-12">
                    <div class="container">
                        <h2 class="color-navy-blue" style="font-size: 25px;">Kalasaala - My Applications</h2>
                        <a href="{{route('kalasaalaNewApplication')}}">
                            <button style="margin-bottom: 40px; float:right; margin-top: -42px;"
                                    type="button" class="btn btn-success">New Application
                            </button>
                        </a>

                        @if(count($KalasaalaApplication) > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Application ID</th>
                                    <th>Art Form</th>
                                    <th>Age Category</th>
                                    <th>Processed Date</th>
                                    <th>Batch Details</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($KalasaalaApplication as $application)
                                    <tr>
                                        <td>KAL21A{{$application->id}}</td>
                                        <td>{{ ucwords(str_replace('_', ' ', strtolower($application->art_form))) }}</td>
                                        <td>{{ ucwords(str_replace('_', ' ', strtolower($application->age_category))) }}</td>
                                        <td>{{ date('M d, Y',strtotime($application->created_at)) }}</td>
                                        <td>TBD</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-danger">
                                <strong>Ohh...!</strong> We dont find any applications.
                            </div>
                        @endif

                        <div class="col-md-12 col-lg-12" style="    margin-top: 50px;">
                            <div class="xs-text-content xs-pr-20">
                                <h2 class="color-navy-blue" style="font-size: 25px;">Need Assistance ?</h2>
                                <p>For any queries and further information email us on info@jignasa.org and contact us at +91 - 97003 30899.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection