@extends('user.master')
@section('content')
@include('user.master_header')
<main class="xs-main" style="margin-top: 150px;">
   <section class="xs-content-section-padding">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="xs-event-banner">
                  <img src="assets/images/event/event-banner.jpg" alt="">
               </div>
               <div class="row">
                  <div class="col-lg-12 xs-event-wraper">
                     <div class="xs-event-content">
                        <h4>SAC - Student Activity Centres</h4>
                        <p>663 million people drink dirty water. Learn how access to clean water can improve health, boost local econom mies. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore ete dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                     </div>


                     <div class="xs-about-feature xs-mb-30">
                        <h3>Our Vission</h3>
                        <p class="lead">The Globian Fund for Charities seeks positive change around the world through support of non-profit organizations dedicated to social, cultural.</p>
                     </div>
                     <div class="xs-horizontal-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" href="#facilities" role="tab">Highlights</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#mapLocation" role="tab">Gallery</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#contactUs" role="tab">Contact us</a>
                           </li>
                        </ul>
                        <!-- .nav-tabs END -->
                        <!-- Tab panes -->
                        <div class="tab-content">
                           <div class="tab-pane fade show active" id="facilities" role="tabpanel">
                              <p>663 million people drink dirty water. Learn how access to clean water can improve health, boost local econom mies. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore ete dolore magna aliqua.</p>
                              <div class="row">
                                 <div class="col-md-6">
                                    <ul class="xs-unorder-list circle green-icon">
                                       <li>Assisting senior consultants in projects </li>
                                       <li>Share best practices and knowledge.</li>
                                       <li>Assisting senior consultants in projects </li>
                                    </ul>
                                 </div>
                                 <div class="col-md-6">
                                    <ul class="xs-unorder-list circle green-icon">
                                       <li>Collaborate with technology, informations security, and business partners </li>
                                       <li>Find and address performance issues.</li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <!-- #facilities END -->
                           <div class="tab-pane" id="mapLocation" role="tabpanel">
                              <div id="xs-map"></div>
                           </div>
                           <!-- #mapLocation END -->
                           <div class="tab-pane" id="contactUs" role="tabpanel">
                              <div class="xs-contact-form-wraper">
                                 <form action="#" method="POST" id="xs-contact-form" class="xs-contact-form">
                                    <div class="input-group">
                                       <input type="text" name="name" id="xs-name" class="form-control" placeholder="Enter Your Name.....">
                                       <div class="input-group-append">
                                          <div class="input-group-text"><i class="fa fa-user"></i></div>
                                       </div>
                                    </div>
                                    <!-- .input-group END -->
                                    <div class="input-group">
                                       <input type="email" name="email" id="xs-email" class="form-control" placeholder="Enter Your Email.....">
                                       <div class="input-group-append">
                                          <div class="input-group-text"><i class="fa fa-envelope-o"></i></div>
                                       </div>
                                    </div>
                                    <!-- .input-group END -->
                                    <div class="input-group massage-group">
                                       <textarea name="massage" placeholder="Enter Your Message....." id="xs-massage" class="form-control" cols="30" rows="10"></textarea>
                                       <div class="input-group-append">
                                          <div class="input-group-text"><i class="fa fa-pencil"></i></div>
                                       </div>
                                    </div>
                                    <!-- .input-group END -->
                                    <button class="btn btn-success" type="submit" id="xs-submit">submit</button>
                                 </form>
                                 <!-- .xs-contact-form #xs-contact-form END -->
                              </div>
                           </div>
                           <!-- #contactUs END -->
                        </div>
                     </div>
                     <!-- End horizontal tab -->


                  </div>

               </div>
            </div>
         </div>
         <!-- .row end -->
      </div>
      <!-- .container end -->
   </section>
   <!-- End event single section -->
</main>
@endsection