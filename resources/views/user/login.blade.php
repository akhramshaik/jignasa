@extends('user.master')
@section('content')
@include('user.master_header')
<main class="xs-main" style="margin-top: 150px;">
   <section class="xs-section-padding ">
      <div class="container">
         @if(Session::has('chk_msg'))
         <div class="alert {{ Session::get('alert-class') }}"> {{ Session::get('chk_msg') }} </div>
         @endif
         <div class="row">
            <div class="col-lg-6">
               <div class="xs-donation-form-wraper" >
                  <div class="xs-heading xs-mb-30">
                     <h2 class="xs-title" style="font-size: 26px;">Login </h2>
                     <span class="xs-separetor v2"></span>
                  </div>
                  <form action="{{ route('loginProcess') }}" method="POST" >
                     <div class="xs-input-group">
                        <label for="xs-donate-name">Email<span class="color-light-red">*</span></label>
                        <input type="email" required="" name="email" class="form-control" placeholder="Your Email">
                     </div>
                     <div class="xs-input-group">
                        <label for="xs-donate-name">Password<span class="color-light-red">*</span></label>
                        <input type="password" required="" name="password" class="form-control" placeholder="Secret">
                     </div>
                     <button type="submit" class="btn btn-success btn-sm">Login </button>
                     <input type="hidden" id="csrf-token" value="{{csrf_token()}}" name="_token">
                     <a href="{{ route('register') }}">
                        <h2 class="xs-title" style="color: #544e4e;font-size: 14px;margin-top: 10px;"> Not a member yet ? Click here to Register </h2>
                     </a>
                     <a href="{{ route('register') }}">
                        <h2 class="xs-title" style="color: #544e4e;font-size: 14px;margin-top: 10px;"> Forgot Password ? </h2>
                     </a>
                  </form>
               </div>
            </div>
            <div class="col-lg-6">
               <section class="xs-section-padding bg-gray" style="padding: 125px 50px;">
                  <div class="container">
                     <div class="row">
<!--                         <div class="xs-donation-form-wraper">
                           <div class="row">
                              <a href="{{ route('fbLogin') }}"><button class="loginBtn loginBtn--facebook"> Login with Facebook </button></a>
                           </div>
                           <span class="xs-separetor v2"></span> 
                           <div class="row">
                              <a href="{{ route('gmailLogin') }}"><button class="loginBtn loginBtn--google">Login with Google </button></a>
                           </div>
                        </div> -->
                     </div>
                  </div>
               </section>
            </div>
         </div>
      </div>
   </section>
   <section class="xs-section-padding" style="padding-bottom: 0px;padding-top: 10px">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-lg-12">
               <div class="xs-text-content xs-pr-20">
                  <p>Note: All information collected here is just...</p>
               </div>
            </div>
         </div>
      </div>
   </section>
</main>
@include('user.master_footer')
@endsection