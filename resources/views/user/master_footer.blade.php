@if(Sentinel::getUser())
      <div class="footer_yaan_home"><i style="font-size: 22px;" class="fa fa-calendar fa-5x" aria-hidden="true"></i> For KALASAALA Registrations <a href="{{ route('kalasaalaApplication') }}"><button style="font-weight: 500;margin-top: 5px;margin-bottom: 5px;padding-top: 10px;padding-bottom: 10px;font-size: 15px;" type="button" class="btn btn-success">Click Here</button></a></div>
@else   
   @if(Sentinel::getUser())
      <div class="footer_yaan_home"><i style="font-size: 22px;" class="fa fa-calendar fa-5x" aria-hidden="true"></i> For KALASAALA Registrations <a href="{{ route('kalasaalaApplication') }}"><button style="font-weight: 500;margin-top: 5px;margin-bottom: 5px;padding-top: 10px;padding-bottom: 10px;font-size: 15px;" type="button" class="btn btn-success">Click Here</button></a></div>
   @else   
      <div class="footer_yaan_home"><i style="font-size: 22px;" class="fa fa-calendar fa-5x" aria-hidden="true"></i> For KALASAALA Registrations <a href="{{ route('login') }}"><button style="font-weight: 500;margin-top: 5px;margin-bottom: 5px;padding-top: 10px;padding-bottom: 10px;font-size: 15px;" type="button" class="btn btn-success">Click Here</button></a></div>
   @endif
@endif



<style type="text/css">
   
   .footer_yaan {
    position: fixed;
    bottom: 0;
    width: 100%;
    background: #28b463;
    line-height: 2;
    text-align: center;
    color: #ffffff;
    font-size: 16px;
    z-index:1000;
}

   
   .footer_yaan_home {
    position: fixed;
    bottom: 0;
    width: 100%;
    background: #ff853f;
    line-height: 2;
    text-align: center;
    color: #ffffff;
    font-size: 24px;
    z-index:1000;
}
</style>
      <footer class="xs-footer-section">

         <div class="container">
            <div class="xs-copyright">
               <div class="row">
                  <div class="col-md-6">
                     <div class="xs-copyright-text">
                        <p>&copy; Copyright 2018 <a href="#">Jignasa</a> - All Right's Reserved</p>
                     </div>
                  </div>

               </div>
            </div>
         </div>
         <div class="xs-back-to-top-wraper">
            <a href="#" class="xs-back-to-top"><i class="fa fa-angle-up"></i></a>
         </div>
      </footer>  