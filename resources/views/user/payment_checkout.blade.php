@extends('user.master')
<style type="text/css">
    .razorpay-payment-button {
        color: #fff;
        float: right;
        background-color: #28a745;
        border-color: #28a745;
        display: inline-block;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        border: 1px solid transparent;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: 0.25rem;
        transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }

    .razorpay-payment-button:hover, .razorpay-payment-button:focus {
        text-decoration: none;
    }

    .razorpay-payment-button:focus, .razorpay-payment-button.focus {
        outline: 0;
        box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
    }
</style>
@section('content')
    @include('user.master_header')
    <main class="xs-main" style="margin-top: 150px;">
        <section class="xs-section-padding ">
            <div class="container">
                @if( strlen($memberInfo->college) <= 0 )
                    <div class="alert alert-warning">Please update your College Name <a href="{{ route('myaccount') }}">
                            Here </a> to enable Payment Button.
                    </div>
                @endif
                @if( strlen($memberInfo->alternate_mobile) <= 0 )
                    <div class="alert alert-warning">Please update your Emergency Contact Number <a
                                href="{{ route('myaccount') }}"> Here </a> to enable Payment Button..
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="xs-text-content xs-pr-20">
                            <h2 class="color-navy-blue" style="font-size: 25px;">Confirmation</h2>
                            <p>Please do validate that your information below is correct. If you have anything to be
                                modified plese <a href="{{ route('myaccount') }}"> Click Here </a> to modify them. All
                                the information collected here is for security reasons only, not for any financial
                                exchange. This information is secured with jignasa and not published in any public
                                domains.</p>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td>Your Name</td>
                            <td>{{ $memberInfo->name }}</td>
                        </tr>
                        <tr>
                            <td>Your School or College Name</td>
                            <td>{{ $memberInfo->college }}</td>
                        </tr>
                        <tr>
                            <td>Your Email</td>
                            <td>{{ $memberInfo->email }}</td>
                        </tr>
                        <tr>
                            <td>Your Mobile</td>
                            <td>{{ $memberInfo->mobile }}</td>
                        </tr>

                        @if( $memberInfo->country == 'india')
                            <tr>
                                <td>Jignasa Yaan Amount</td>
                                <td> 800</td>
                            </tr>
                            <tr>
                                <td>Payment Handling Charges + GST</td>
                                <td> 20</td>
                            </tr>
                        @else
                            <tr>
                                <td>Jignasa Yaan Amount</td>
                                <td> 1000</td>
                            </tr>
                            <tr>
                                <td>Payment Handling Charges + GST</td>
                                <td> 20</td>
                            </tr>
                        @endif

                        </tbody>
                    </table>

{{ $memberInfo->created_at}}eeee
                    <div class="col-md-12 col-lg-12">
                        <div class="xs-text-content xs-pr-20">
                            @if( strlen($memberInfo->college) >= 1 && strlen($memberInfo->alternate_mobile) >= 1 )
                                @if( $memberInfo->country == 'india')
                                    <form action="{{ route('paymentProcess') }}" method="POST">
                                        <input type="hidden" name="user_id" value="{{ $memberInfo->member_id }}">
                                        <input type="hidden" id="csrf-token" value="{{csrf_token()}}" name="_token">
                                        @if(Sentinel::getUser()->last_name == '1' )
                                            <button type="button" disabled style="float: right;"
                                                    class="btn disabled btn-success">Payment Completed
                                            </button>
                                        @else
                                            <button type="submit" style="float: right;" class="btn btn-success">Pay Rs.
                                                820
                                            </button>
                                        @endif
                                    </form>
                                @else
                                    @if(Sentinel::getUser()->last_name == '1' )
                                        <button type="button" disabled style="float: right;"
                                                class="btn disabled btn-success">Payment Completed
                                        </button>
                                    @else
                                        <form action="{!!route('paymentProcessRazar')!!}" method="POST">
                                            <script src="https://checkout.razorpay.com/v1/checkout.js"
                                                    data-key="{{ env('RAZOR_KEY') }}"
                                                    data-amount="1000"
                                                    data-currency="USD"
                                                    data-buttontext="Pay $10"
                                                    data-name="Jignasa"
                                                    data-description="Jignasa - Kalasaala Payment"
                                                    data-image="https://jignasa.org/assets/images/logo.png"
                                                    data-prefill.name="{{$memberInfo->name}}"
                                                    data-prefill.email="{{$memberInfo->email}}"
                                                    data-theme.color="#ff7529">
                                            </script>
                                            <input type="hidden" name="_token" value="{!!csrf_token()!!}">
                                            @endif
                                        </form>
                                    @endif
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="xs-section-padding" style="padding: 10px 0;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="xs-text-content xs-pr-20">
                            <h2 class="color-navy-blue" style="font-size: 25px;">Jignasa Yaan 2019 - Rules and
                                Regulations</h2>
                            <div class="col-md-12">
                                <ul class="xs-unorder-list circle green-icon">


                                    <li>All the students must abide by the disciplinary code of conduct said/informed by
                                        the organizing committee JY19. Any student violating such rules shall be
                                        subjected to disciplinary action and leads to suspension of the student from the
                                        YAAN WIE, and the decision of disciplinary committee is final.
                                    </li>
                                    <li>The student must occupy his/her seat either in bus/train as directed by the seat
                                        arrangement committee without any debate, argument or questioning.
                                    </li>
                                    <li>Participants are restrained to sit/stand at foot board of Train or Bus
                                        throughout the journey.
                                    </li>
                                    <li>Under any circumstances, students should not try to pull the train chain
                                        (emergency break) without any supervision of organizing committee and will be
                                        taken serious action against him/her and it may lead to debarring from his/her
                                        college.
                                    </li>
                                    <li>The student must be in the group and engage in the activities of the group very
                                        actively without fail and should not create problems to others.
                                    </li>
                                    <li>Food should not be wasted “TAKE ALL YOU CAN EAT,BUT EAT ALL YOU TAKE”. After
                                        eating is should be thrown in dustbins.
                                    </li>
                                    <li>The student shall not argue/debate for asking him/her to be in particular group
                                        with any organizing committee mentor and the decision of the organizing
                                        committee is final.
                                    </li>
                                    <li>The student movement IN or OUT of the IIMK Campus during the JIGNASA YAAN 19
                                        shall be as directed by the organizing committee; no individual plans are
                                        entertained as it involves the security of contingent.
                                    </li>
                                    <li>The students should involve in all the activities conducted by JIGNASA with full
                                        enthusiasm. Even improper engagement and lack of enthusiasm is considered as
                                        indiscipline.
                                    </li>
                                    <li>The students should not take any alcohol/smoking/ intoxicants/tobacco products
                                        throughout the JIGNASA YAAN 19. Any student violating these rules shall be
                                        subject to disciplinary action and leads to suspension of the student from the
                                        YAAN and debarring from the college, and the decision of disciplinary committee
                                        is final. The same shall be communicated to the college and student parents.
                                    </li>
                                    <li>Any student should not tease, rag or cause any sort of disturbance to the
                                        opposite gender. Such issues shall be taken very seriously against him/her and
                                        it may lead to debarring from his/her college.
                                    </li>
                                    <li>A student should not argue debate or comment any mentor or organizing committee
                                        member, such thing is considered as in disciplinary action.
                                    </li>
                                    <li>All the students must oblige by the committees, committee heads without fail.
                                    </li>
                                    <li>The students should not comment or speak anything negative regarding their
                                        institute or associate institute or JIGNASA.
                                    </li>
                                    <li>The students shall restrain themselves from doing unauthorized or unnecessary
                                        issues or activities for the smooth conduction of JIGNASA YAAN 17.
                                    </li>
                                    <li>The students shall not be allowed to any sport or a casual view to any water
                                        body in the due course of journey. Please keep yourself at least 50 meters
                                        away..We wish to have another JIGNASA YAAN in the next semester with you.
                                    </li>
                                    <li>We do not provide any food or medication during the journey in train/bus it is
                                        YAANIK’S choice and you should make necessary arrangements for the same.
                                    </li>
                                    <li>You can bring to Organizers’ notice any disturbance or problem and it will
                                        rectified as early as possible.
                                    </li>
                                    <li>Consent letter from Parent/Guardian is obligatory to participate in JIGNASAYAAN
                                        19.
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12" style="    margin-top: 50px;">
                        <div class="xs-text-content xs-pr-20">
                            <h2 class="color-navy-blue" style="font-size: 25px;">Note</h2>
                            <p>We are not military personnel and we do not like to impose such discipline upon you. We
                                exercise these rules in the student friendly environment. However, our student
                                friendliness has a limit to assure and ensure safe & secure Knowledge journey with
                                purpose to all the YAANIKS who joined us on JIGNASA YAAN 18. It is our and your
                                responsibility to ensure healthy and secure environment to beloved JIGNASA YAAN 18
                                fraternity. Hope You oblige these rules with heart having breadth pointed to
                                infinity.</p>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-12" style="    margin-top: 50px;">
                        <div class="xs-text-content xs-pr-20">
                            <h2 class="color-navy-blue" style="font-size: 25px;">Refund Policy</h2>
                            <p>Returns and Refunds Policy. ONLY 30% OF TOTAL FARE WILL BE REFUNDED IF CANCELLED BEFORE
                                1ST DECEMBER 2019 AND NO REFUND WILL BE GIVEN AFTER THIS .</p>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12" style="    margin-top: 50px;">
                        <div class="xs-text-content xs-pr-20">
                            <h2 class="color-navy-blue" style="font-size: 25px;">Need Assistance ?</h2>
                            <p>For any queries and further information email us on info@jignasa.org and contact us at
                                +91 - 94916 89304.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    @include('user.master_footer')
@endsection