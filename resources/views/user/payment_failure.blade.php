@extends('user.master')
@section('content')
@include('user.master_header')
<main class="xs-main" style="margin-top: 150px;">
   <section class="xs-section-padding ">
      <div class="container">
         @if(Session::has('chk_msg'))
         <div class="alert {{ Session::get('alert-class') }}"> {{ Session::get('chk_msg') }} </div>
         @endif
            <center>
              <span>
              <i style="padding-bottom: 15px;font-size: 90px;color: #ec3a50;" class="fa fa-frown-o fa-5x" aria-hidden="true"></i>
              </span> 
            </center>
         <div class="alert alert-danger">
            <center>
            <strong>Oh...!</strong> Your Payment was unsuccesfull. Try again later.
            </center>
         </div>

      </div>
   </section>


   <section class="xs-section-padding" style="padding: 10px 0;">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-lg-12">
            <div class="xs-text-content xs-pr-20">
               <h2 class="color-navy-blue" style="font-size: 25px;">Need Assistance ?</h2>
               <p>For any queries and further information email us on info@jignasa.org and contact us at +91 - 97003 30899.</p>


            </div>
         </div>

      </div>
   </div>
</section>
</main>
@endsection