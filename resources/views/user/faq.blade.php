@extends('user.master')
@section('content')
@include('user.master_header')
<section class="xs-welcome-slider">
   <div class="xs-banner-slider owl-carousel">
      <div class="xs-welcome-content" style="background-image: url(assets/images/slider/slider_1.jpg);">
         <div class="container">
            <div class="xs-welcome-wraper color-white">
               <h2>Hunger is stalking the globe</h2>
               <p>Hundreds of thousands of children experiencing or witnessing assault <br> and other gender-based violence.</p>
               <div class="xs-btn-wraper">
                  <a href="#" class="btn btn-outline-primary">
                  join us now
                  </a>
                  <a href="#" class="btn btn-primary">
                  <span class="badge"><i class="fa fa-heart"></i></span> Donate Now
                  </a>
               </div>
            </div>
         </div>
         <div class="xs-black-overlay"></div>
      </div>
      <div class="xs-welcome-content" style="background-image: url(assets/images/slider/slider_2.jpg);">
         <div class="container">
            <div class="xs-welcome-wraper color-white">
               <img src="assets/images/welcome-text-1.png" alt="">
               <h2>Hunger is stalking the globe</h2>
               <p>Hundreds of thousands of children experiencing or witnessing assault <br> and other gender-based violence.</p>
               <div class="xs-btn-wraper">
                  <a href="#" class="btn btn-outline-primary">
                  join us now
                  </a>
                  <a href="#" class="btn btn-primary">
                  <span class="badge"><i class="fa fa-heart"></i></span> Donate Now
                  </a>
               </div>
            </div>
         </div>
         <div class="xs-black-overlay"></div>
      </div>
      <div class="xs-welcome-content" style="background-image: url(assets/images/slider/slider_3.jpg);">
         <div class="container">
            <div class="xs-welcome-wraper color-white">
               <img src="assets/images/welcome-text-1.png" alt="">
               <h2>Hunger is stalking the globe</h2>
               <p>Hundreds of thousands of children experiencing or witnessing assault <br> and other gender-based violence.</p>
               <div class="xs-btn-wraper">
                  <a href="#" class="btn btn-outline-primary">
                  join us now
                  </a>
                  <a href="#" class="btn btn-primary">
                  <span class="badge"><i class="fa fa-heart"></i></span> Donate Now
                  </a>
               </div>
            </div>
         </div>
         <div class="xs-black-overlay"></div>
      </div>
   </div>
</section>
<section class="waypoint-tigger xs-section-padding">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-lg-6">
            <div class="xs-archive-content">
               <div class="xs-heading">
                  <h2 class="xs-line-title">Our Missions</h2>
                  <h3 class="xs-title big" data-title="Archived">Jignasa</h3>
               </div>
               <p>We register and regulate charities in England and Wales, to ensure that the public can support charities with confidence. As registrar, we are responsible for maintaining an accurate.</p>
               <h5>Our strategic priorities up to 2018 are:</h5>
               <ul class="xs-unorder-list arrow">
                  <li>Protecting charities from abuse or mismanagement</li>
                  <li>Enabling trustees to run their charities effectively</li>
                  <li>Encouraging greater transparency and accountability</li>
               </ul>
               <ul class="xs-funfact-list">
                  <li>
                     <p>
                        <span class="number-percentage-count number-percentage" data-value="132" data-animation-duration="3500">0</span><sup> + </sup>
                     </p>
                     <span>Primary Schools</span>
                  </li>
                  <li>
                     <p>
                        <span class="number-percentage-count number-percentage" data-value="19" data-animation-duration="3500">0</span><sup> + </sup>
                     </p>
                     <span>Hospitals</span>
                  </li>
                  <li>
                     <p>
                        <span class="number-percentage-count number-percentage" data-value="90" data-animation-duration="3500">0</span><sup> + </sup>
                     </p>
                     <span>Volunteers</span>
                  </li>
                  <li>
                     <p>
                        <span class="number-percentage-count number-percentage" data-value="27" data-animation-duration="3500">0</span><sup> + </sup>
                     </p>
                     <span>Winning Awards</span>
                  </li>
                  <div class="clearfix"></div>
               </ul>
            </div>
         </div>
         <div class="col-md-12 col-lg-6 row xs-archive-image">
            <div class="col-md-12 xs-mb-30">
               <img src="assets/images/archive-img-1.jpg" alt="" class="rounded">
            </div>
            <div class="col-md-6 col-sm-6">
               <img src="assets/images/archive-img-2.jpg" alt="" class="rounded">
            </div>
            <div class="col-md-6 col-sm-6">
               <img src="assets/images/archive-img-3.jpg" alt="" class="rounded">
            </div>
         </div>
      </div>
   </div>
</section>
<section class="bg-navy-blue">
   <div class="container-fulid">
      <div class="xs-feature-content">
         <h2 class="color-white">CharityPress are <span> charity </span> activities are taken place around the world, lets contribute.</h2>
      </div>
   </div>
</section>
<section class="xs-section-padding">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-lg-6">
            <div class="xs-text-content xs-pr-20">
               <h2 class="color-navy-blue">Welcome to CharityPress please rise your hand</h2>
               <p>The CharityPress community was named a “Top 25 Best Global Philanthropist” by Barron’s. We beat Oprah. And, Mashable named CharityPress something like “the best place to raise money online for your favorite causes.”</p>
               <blockquote>
                  If you don't understand how fast and easy it is to so long for your favorite charity on FundPress, please try it. <span>How it works</span> page, <span>Contact us</span>.
               </blockquote>
               <a href="#" class="btn btn-primary">
               <span class="badge"><i class="fa fa-heart"></i></span> Donate Now
               </a>
            </div>
         </div>
         <div class="col-md-6 col-lg-3">
            <div class="xs-feature-image">
               <img src="assets/images/features_1.jpg" alt="">
            </div>
         </div>
         <div class="col-md-6 col-lg-3">
            <div class="xs-feature-image">
               <img src="assets/images/features_2.jpg" alt="">
            </div>
         </div>
      </div>
   </div>
</section>
<section class="xs-content-section-padding">
   <div class="container">
      <div class="xs-heading row col-lg-10 xs-mb-70 text-center mx-auto">
         <h2 class="xs-mb-0 xs-title">Our Journey <span class="color-green">So far</span> .</h2>
      </div>
      <div class="row">
         <div class="col-md-6 col-lg-3">
            <div class="xs-service-promo">
               <span class="icon-water color-orange"></span>
               <h5>Pure Water <br>For Poor People</h5>
            </div>
         </div>
         <div class="col-md-6 col-lg-3">
            <div class="xs-service-promo">
               <span class="icon-groceries color-red"></span>
               <h5>Healty Food <br>For Poor People</h5>
            </div>
         </div>
         <div class="col-md-6 col-lg-3">
            <div class="xs-service-promo">
               <span class="icon-heartbeat color-purple"></span>
               <h5>Medical <br>Facilities for People</h5>
            </div>
         </div>
         <div class="col-md-6 col-lg-3">
            <div class="xs-service-promo">
               <span class="icon-open-book color-green"></span>
               <h5>Pure Education <br>For Every Children</h5>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="bg-gray waypoint-tigger xs-section-padding">
   <div class="container">
      <div class="xs-heading row xs-mb-60">
         <div class="col-md-9 col-xl-9">
            <h2 class="xs-title">Recent Hapennings</h2>
            <span class="xs-separetor dashed"></span>
            <p>FundPress has built a platform focused on aiding entrepreneurs, startups, and <br> companies raise capital from anyone.</p>
         </div>
         <div class="col-xl-3 col-md-3 xs-btn-wraper">
            <a href="" class="btn btn-primary">View All</a>
         </div>
      </div>
      <div class="row">
         <div class="col-lg-4 col-md-6">
            <div class="xs-popular-item xs-box-shadow">
               <div class="xs-item-header">
                  <img src="assets/images/causes/causes_4.jpg" alt="">
               </div>
               <div class="xs-item-content">
                  <ul class="xs-simple-tag xs-mb-20">
                     <li><a href="">Food</a></li>
                  </ul>
                  <a href="#" class="xs-post-title xs-mb-30">Splash Drone 3 a Fully Waterproof Drone that floats</a>
               </div>
            </div>
         </div>
         <div class="col-lg-4 col-md-6">
            <div class="xs-popular-item xs-box-shadow">
               <div class="xs-item-header">
                  <img src="assets/images/causes/causes_5.jpg" alt="">
               </div>
               <div class="xs-item-content">
                  <ul class="xs-simple-tag xs-mb-20">
                     <li><a href="">Health</a></li>
                  </ul>
                  <a href="#" class="xs-post-title xs-mb-30">The Read Read: Braille Literacy Tool for the Blind</a>
               </div>
            </div>
         </div>
         <div class="col-lg-4 col-md-6">
            <div class="xs-popular-item xs-box-shadow">
               <div class="xs-item-header">
                  <img src="assets/images/causes/causes_6.jpg" alt="">
               </div>
               <div class="xs-item-content">
                  <ul class="xs-simple-tag xs-mb-20">
                     <li><a href="">Education</a></li>
                  </ul>
                  <a href="#" class="xs-post-title xs-mb-30">BuildOne: $99 3D Printer w/ WiFi and Auto Bed Leveling!</a>
               </div>
            </div>
         </div>
         <div class="col-lg-4 col-md-6">
            <div class="xs-popular-item xs-box-shadow">
               <div class="xs-item-header">
                  <img src="assets/images/causes/causes_4.jpg" alt="">
               </div>
               <div class="xs-item-content">
                  <ul class="xs-simple-tag xs-mb-20">
                     <li><a href="">Food</a></li>
                  </ul>
                  <a href="#" class="xs-post-title xs-mb-30">Splash Drone 3 a Fully Waterproof Drone that floats</a>
               </div>
            </div>
         </div>
         <div class="col-lg-4 col-md-6">
            <div class="xs-popular-item xs-box-shadow">
               <div class="xs-item-header">
                  <img src="assets/images/causes/causes_5.jpg" alt="">
               </div>
               <div class="xs-item-content">
                  <ul class="xs-simple-tag xs-mb-20">
                     <li><a href="">Health</a></li>
                  </ul>
                  <a href="#" class="xs-post-title xs-mb-30">The Read Read: Braille Literacy Tool for the Blind</a>
               </div>
            </div>
         </div>
         <div class="col-lg-4 col-md-6">
            <div class="xs-popular-item xs-box-shadow">
               <div class="xs-item-header">
                  <img src="assets/images/causes/causes_6.jpg" alt="">
               </div>
               <div class="xs-item-content">
                  <ul class="xs-simple-tag xs-mb-20">
                     <li><a href="">Education</a></li>
                  </ul>
                  <a href="#" class="xs-post-title xs-mb-30">BuildOne: $99 3D Printer w/ WiFi and Auto Bed Leveling!</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="parallax-window xs-become-a-volunteer xs-section-padding" style="background-image: url('assets/images/backgrounds/volunteer-bg.jpg')">
   <div class="container">
      <div class="row">
         <div class="col-md-8 col-lg-7">
            <div class="xs-volunteer-form-wraper volunteer-version-3">
               <i class="icon-support icon-watermark"></i>
               <h2>Become a Member</h2>
               <p>It only takes a minute to set up a campaign. Decide what to do. Pick a name. Pick a photo. And just like that, you’ll be ready to start raising money.</p>
               <form action="#" method="POST" id="volunteer-form" class="xs-volunteer-form">
                  <div class="row">
                     <div class="col-lg-6">
                        <input type="text" id="volunteer_name" class="form-control" placeholder="Your Name">
                     </div>
                     <div class="col-lg-6">
                        <input type="email" id="volunteer_email" class="form-control" placeholder="Your Email">
                     </div>
                     <div class="col-lg-6">
                        <select name="branch" class="form-control" id="volunteer_brach">
                           <option value="">Select</option>
                           <option value="">Branch</option>
                           <option value="">New york</option>
                           <option value="">washington</option>
                        </select>
                     </div>
                     <div class="col-lg-6 xs-mb-20">
                        <div class="xs-fileContainer">
                           <input type="file" id="volunteer_cv" class="form-control" name="file">
                           <label for="volunteer_cv">Upload Your CV</label>
                        </div>
                     </div>
                  </div>
                  <textarea name="massage" id="massage" placeholder="Enter your massage" cols="30" class="form-control" rows="10"></textarea>
                  <button type="submit" class="btn btn-primary bg-green">apply now</button>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="xs-section-padding">
   <div class="container">
      <div class="xs-heading row xs-mb-60">
         <div class="col-md-9 col-xl-9">
            <h2 class="xs-title">UpComming Events</h2>
            <span class="xs-separetor dashed"></span>
            <p>FundPress has built a platform focused on aiding entrepreneurs, startups, and <br> companies raise capital from anyone.</p>
         </div>
      </div>
      <div class="row">
         <div class="col-lg-6 row xs-single-event event-green">
            <div class="col-md-5">
               <div class="xs-event-image">
                  <img src="assets/images/event/event_1.jpg" alt="">
                  <div class="xs-entry-date">
                     <span class="entry-date-day">27</span>
                     <span class="entry-date-month">dec</span>
                  </div>
                  <div class="xs-black-overlay"></div>
               </div>
            </div>
            <div class="col-md-7">
               <div class="xs-event-content">
                  <a href="#">Raspberry velbet</a>
                  <p>In a time of overwhelming emotions, sadness, and pain, obligations.</p>
                  <div class="xs-countdown-timer" data-countdown="2020/01/24"></div>
                  <a href="#" class="btn btn-primary">
                  Learn More
                  </a>
               </div>
            </div>
         </div>
         <div class="col-lg-6 row xs-single-event event-purple">
            <div class="col-md-5">
               <div class="xs-event-image">
                  <img src="assets/images/event/event_2.jpg" alt="">
                  <div class="xs-entry-date">
                     <span class="entry-date-day">15</span>
                     <span class="entry-date-month">aug</span>
                  </div>
                  <div class="xs-black-overlay"></div>
               </div>
            </div>
            <div class="col-md-7">
               <div class="xs-event-content">
                  <a href="#">Arsenal, the intelligent.</a>
                  <p>In a time of overwhelming emotions, sadness, and pain, obligations.</p>
                  <div class="xs-countdown-timer" data-countdown="2020/08/24"></div>
                  <a href="#" class="btn btn-primary">
                  Learn More
                  </a>
               </div>
            </div>
         </div>
         <div class="col-lg-6 row xs-single-event event-red">
            <div class="col-md-5">
               <div class="xs-event-image">
                  <img src="assets/images/event/event_3.jpg" alt="">
                  <div class="xs-entry-date">
                     <span class="entry-date-day">24</span>
                     <span class="entry-date-month">jan</span>
                  </div>
                  <div class="xs-black-overlay"></div>
               </div>
            </div>
            <div class="col-md-7">
               <div class="xs-event-content">
                  <a href="#">Waterproof drone that</a>
                  <p>In a time of overwhelming emotions, sadness, and pain, obligations.</p>
                  <div class="xs-countdown-timer" data-countdown="2019/05/24"></div>
                  <a href="#" class="btn btn-primary">
                  Learn More
                  </a>
               </div>
            </div>
         </div>
         <div class="col-lg-6 row xs-single-event event-blue">
            <div class="col-md-5">
               <div class="xs-event-image">
                  <img src="assets/images/event/event_4.jpg" alt="">
                  <div class="xs-entry-date">
                     <span class="entry-date-day">23</span>
                     <span class="entry-date-month">jun</span>
                  </div>
                  <div class="xs-black-overlay"></div>
               </div>
            </div>
            <div class="col-md-7">
               <div class="xs-event-content">
                  <a href="">Braille Literacy Tool for.</a>
                  <p>In a time of overwhelming emotions, sadness, and pain, obligations.</p>
                  <div class="xs-countdown-timer" data-countdown="2020/02/24"></div>
                  <a href="#" class="btn btn-primary">
                  Learn More
                  </a>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="bg-gray xs-partner-section" style="background-image: url('assets/images/map.png');">
   <div class="container">
      <div class="row">
         <div class="col-lg-5">
            <div class="xs-partner-content">
               <div class="xs-heading xs-mb-40">
                  <h2 class="xs-mb-0 xs-title">Trusted by the biggest <span class="color-green">brand.</span></h2>
               </div>
               <p>In-kind donations from our donors and partners allow charity: water to pass 100% of public donations straight to water projects. We are deeply grateful for those who have surprised us with their generosity. A big thanks to the following companies and people who have helped make charity: water’s work possible.</p>
               <a href="#" class="btn btn-primary bg-orange">
               join us now
               </a>
            </div>
         </div>
         <div class="col-lg-7">
            <ul class="fundpress-partners">
               <li><a href="#"><img src="assets/images/partner/client_1.png" alt=""></a></li>
               <li><a href="#"><img src="assets/images/partner/client_2.png" alt=""></a></li>
               <li><a href="#"><img src="assets/images/partner/client_3.png" alt=""></a></li>
               <li><a href="#"><img src="assets/images/partner/client_4.png" alt=""></a></li>
               <li><a href="#"><img src="assets/images/partner/client_5.png" alt=""></a></li>
            </ul>
         </div>
      </div>
   </div>
</section>
@endsection