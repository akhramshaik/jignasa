@extends('user.master')
@section('content')
@include('user.master_header')
<main class="xs-main" style="margin-top: 150px;">
   <section class="xs-section-padding ">
      <div class="container">
         @if(Session::has('chk_msg'))
         <div class="alert {{ Session::get('alert-class') }}"> {{ Session::get('chk_msg') }} </div>
         <div class="row" style="margin: auto;width: 50%;text-align: center;">
                  <div class="xs-heading xs-mb-40">
                     <h2 style="font-size: 30px;" class="xs-mb-0 xs-title">For Kalasaala 2021  <span class="color-green">Registrations</span></h2> <a href="{{ route('kalasaalaApplication') }}" class="btn btn-primary bg-orange"> Click Here</a>
            </div>
         </div>
         @endif

         <div class="row">
            <div class="col-lg-6">
               <div class="xs-donation-form-wraper" >
                  <div class="xs-heading xs-mb-30">
                     <h2 class="xs-title" style="font-size: 26px;">My Account</h2>
                     <span class="xs-separetor v2"></span>
                  </div>
                  <form action="{{ route('editProcess') }}" method="POST" autocomplete="off">
                     <div class="xs-input-group">
                        <label for="xs-donate-name">Name<span class="color-light-red">*</span></label>
                        <input type="text" value="{{ $memberData->name }}" required name="member_name" class="form-control" placeholder="Your Name">
                     </div>
                     <div class="xs-input-group">
                        <label for="sel1">Gender </label>
                        <select class="form-control member_gender" name="member_gender">
                           <option value="male">Male</option>
                           <option value="female">Female</option>
                           <option value="other">Other</option>
                        </select>
                     </div>


     


                                          <div class="xs-input-group date">
                        <label for="sel1">Date Of Birth<span class="color-light-red">*</span> </label>

                    <input type="text" value="{{ $memberData->dob_user }}" placeholder="Your Date Of Birth" name="dob_user" class="form-control" id="js-date">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
                     </div>


                     <div class="xs-input-group">
                        <label for="xs-donate-name">Email<span class="color-light-red">*</span></label>
                        <input type="email" value="{{ $memberData->email }}" required name="member_email" class="form-control" placeholder="Your Email">
                     </div>
                     <div class="xs-input-group">
                        <label for="xs-donate-name">School or College Name<span class="color-light-red">*</span></label>
                        <input type="text" value="{{ $memberData->college }}" required name="college_name" class="form-control" placeholder="Your School or College Name">
                     </div>

                                          <div class="xs-input-group">
                        <label for="xs-donate-name">Current Location<span class="color-light-red">*</span></label>
                        <input type="text" value="{{ $memberData->current_location }}" required name="current_location" class="form-control" placeholder="Your Village or City">
                     </div>

        <!--                                   <div class="xs-input-group">
                        <label for="xs-donate-name">Country<span class="color-light-red">*</span></label>
                        <input type="text" value="{{ $memberData->country }}" required name="country" class="form-control" placeholder="Your Country">
                     </div>
 -->

                     <div class="xs-input-group">
                        <label for="sel1">Country <span class="color-light-red">*</span></label>
                        <select class="form-control country" name="country" required>
   <option value="Afganistan">Afghanistan</option>
   <option value="Albania">Albania</option>
   <option value="Algeria">Algeria</option>
   <option value="American_Samoa">American Samoa</option>
   <option value="Andorra">Andorra</option>
   <option value="Angola">Angola</option>
   <option value="Anguilla">Anguilla</option>
   <option value="Antigua_Barbuda">Antigua & Barbuda</option>
   <option value="Argentina">Argentina</option>
   <option value="Armenia">Armenia</option>
   <option value="Aruba">Aruba</option>
   <option value="Australia">Australia</option>
   <option value="Austria">Austria</option>
   <option value="Azerbaijan">Azerbaijan</option>
   <option value="Bahamas">Bahamas</option>
   <option value="Bahrain">Bahrain</option>
   <option value="Bangladesh">Bangladesh</option>
   <option value="Barbados">Barbados</option>
   <option value="Belarus">Belarus</option>
   <option value="Belgium">Belgium</option>
   <option value="Belize">Belize</option>
   <option value="Benin">Benin</option>
   <option value="Bermuda">Bermuda</option>
   <option value="Bhutan">Bhutan</option>
   <option value="Bolivia">Bolivia</option>
   <option value="Bonaire">Bonaire</option>
   <option value="Bosnia_Herzegovina">Bosnia & Herzegovina</option>
   <option value="Botswana">Botswana</option>
   <option value="Brazil">Brazil</option>
   <option value="British_Indian_Ocean_Ter">British Indian Ocean Ter</option>
   <option value="Brunei">Brunei</option>
   <option value="Bulgaria">Bulgaria</option>
   <option value="Burkina_Faso">Burkina Faso</option>
   <option value="Burundi">Burundi</option>
   <option value="Cambodia">Cambodia</option>
   <option value="Cameroon">Cameroon</option>
   <option value="Canada">Canada</option>
   <option value="Canary_Islands">Canary Islands</option>
   <option value="Cape_Verde">Cape Verde</option>
   <option value="Cayman_Islands">Cayman Islands</option>
   <option value="Central_African_Republic">Central African Republic</option>
   <option value="Chad">Chad</option>
   <option value="Channel_Islands">Channel Islands</option>
   <option value="Chile">Chile</option>
   <option value="China">China</option>
   <option value="Christmas_Island">Christmas Island</option>
   <option value="Cocos_Island">Cocos Island</option>
   <option value="Colombia">Colombia</option>
   <option value="Comoros">Comoros</option>
   <option value="Congo">Congo</option>
   <option value="Cook_Islands">Cook Islands</option>
   <option value="Costa_Rica">Costa Rica</option>
   <option value="Cote_DIvoire">Cote DIvoire</option>
   <option value="Croatia">Croatia</option>
   <option value="Cuba">Cuba</option>
   <option value="Curaco">Curacao</option>
   <option value="Cyprus">Cyprus</option>
   <option value="Czech_Republic">Czech Republic</option>
   <option value="Denmark">Denmark</option>
   <option value="Djibouti">Djibouti</option>
   <option value="Dominica">Dominica</option>
   <option value="Dominican_Republic">Dominican Republic</option>
   <option value="East_Timor">East Timor</option>
   <option value="Ecuador">Ecuador</option>
   <option value="Egypt">Egypt</option>
   <option value="El_Salvador">El Salvador</option>
   <option value="Equatorial_Guinea">Equatorial Guinea</option>
   <option value="Eritrea">Eritrea</option>
   <option value="Estonia">Estonia</option>
   <option value="Ethiopia">Ethiopia</option>
   <option value="Falkland_Islands">Falkland Islands</option>
   <option value="Faroe_Islands">Faroe Islands</option>
   <option value="Fiji">Fiji</option>
   <option value="Finland">Finland</option>
   <option value="France">France</option>
   <option value="French_Guiana">French Guiana</option>
   <option value="French_Polynesia">French Polynesia</option>
   <option value="French_Southern Ter">French Southern Ter</option>
   <option value="Gabon">Gabon</option>
   <option value="Gambia">Gambia</option>
   <option value="Georgia">Georgia</option>
   <option value="Germany">Germany</option>
   <option value="Ghana">Ghana</option>
   <option value="Gibraltar">Gibraltar</option>
   <option value="Great_Britain">Great Britain</option>
   <option value="Greece">Greece</option>
   <option value="Greenland">Greenland</option>
   <option value="Grenada">Grenada</option>
   <option value="Guadeloupe">Guadeloupe</option>
   <option value="Guam">Guam</option>
   <option value="Guatemala">Guatemala</option>
   <option value="Guinea">Guinea</option>
   <option value="Guyana">Guyana</option>
   <option value="Haiti">Haiti</option>
   <option value="Hawaii">Hawaii</option>
   <option value="Honduras">Honduras</option>
   <option value="Hong_Kong">Hong Kong</option>
   <option value="Hungary">Hungary</option>
   <option value="Iceland">Iceland</option>
   <option value="Indonesia">Indonesia</option>
   <option value="india">India</option>
   <option value="Iran">Iran</option>
   <option value="Iraq">Iraq</option>
   <option value="Ireland">Ireland</option>
   <option value="Isle_of_Man">Isle of Man</option>
   <option value="Israel">Israel</option>
   <option value="Italy">Italy</option>
   <option value="Jamaica">Jamaica</option>
   <option value="Japan">Japan</option>
   <option value="Jordan">Jordan</option>
   <option value="Kazakhstan">Kazakhstan</option>
   <option value="Kenya">Kenya</option>
   <option value="Kiribati">Kiribati</option>
   <option value="Korea_North">Korea North</option>
   <option value="Korea_Sout">Korea South</option>
   <option value="Kuwait">Kuwait</option>
   <option value="Kyrgyzstan">Kyrgyzstan</option>
   <option value="Laos">Laos</option>
   <option value="Latvia">Latvia</option>
   <option value="Lebanon">Lebanon</option>
   <option value="Lesotho">Lesotho</option>
   <option value="Liberia">Liberia</option>
   <option value="Libya">Libya</option>
   <option value="Liechtenstein">Liechtenstein</option>
   <option value="Lithuania">Lithuania</option>
   <option value="Luxembourg">Luxembourg</option>
   <option value="Macau">Macau</option>
   <option value="Macedonia">Macedonia</option>
   <option value="Madagascar">Madagascar</option>
   <option value="Malaysia">Malaysia</option>
   <option value="Malawi">Malawi</option>
   <option value="Maldives">Maldives</option>
   <option value="Mali">Mali</option>
   <option value="Malta">Malta</option>
   <option value="Marshall_Islands">Marshall Islands</option>
   <option value="Martinique">Martinique</option>
   <option value="Mauritania">Mauritania</option>
   <option value="Mauritius">Mauritius</option>
   <option value="Mayotte">Mayotte</option>
   <option value="Mexico">Mexico</option>
   <option value="Midway_Islands">Midway Islands</option>
   <option value="Moldova">Moldova</option>
   <option value="Monaco">Monaco</option>
   <option value="Mongolia">Mongolia</option>
   <option value="Montserrat">Montserrat</option>
   <option value="Morocco">Morocco</option>
   <option value="Mozambique">Mozambique</option>
   <option value="Myanmar">Myanmar</option>
   <option value="Nambia">Nambia</option>
   <option value="Nauru">Nauru</option>
   <option value="Nepal">Nepal</option>
   <option value="Netherland_Antilles">Netherland Antilles</option>
   <option value="Netherlands">Netherlands (Holland, Europe)</option>
   <option value="Nevis">Nevis</option>
   <option value="New_Caledonia">New Caledonia</option>
   <option value="New_Zealand">New Zealand</option>
   <option value="Nicaragua">Nicaragua</option>
   <option value="Niger">Niger</option>
   <option value="Nigeria">Nigeria</option>
   <option value="Niue">Niue</option>
   <option value="Norfolk_Island">Norfolk Island</option>
   <option value="Norway">Norway</option>
   <option value="Oman">Oman</option>
   <option value="Pakistan">Pakistan</option>
   <option value="Palau_Island">Palau Island</option>
   <option value="Palestine">Palestine</option>
   <option value="Panama">Panama</option>
   <option value="Papua_New_Guinea">Papua New Guinea</option>
   <option value="Paraguay">Paraguay</option>
   <option value="Peru">Peru</option>
   <option value="Phillipines">Philippines</option>
   <option value="Pitcairn_Island">Pitcairn Island</option>
   <option value="Poland">Poland</option>
   <option value="Portugal">Portugal</option>
   <option value="Puerto_Rico">Puerto Rico</option>
   <option value="Qatar">Qatar</option>
   <option value="Republic_of_Montenegro">Republic of Montenegro</option>
   <option value="Republic_of_Serbia">Republic of Serbia</option>
   <option value="Reunion">Reunion</option>
   <option value="Romania">Romania</option>
   <option value="Russia">Russia</option>
   <option value="Rwanda">Rwanda</option>
   <option value="St_Barthelemy">St Barthelemy</option>
   <option value="St_Eustatius">St Eustatius</option>
   <option value="St_Helena">St Helena</option>
   <option value="St_Kitts-Nevis">St Kitts-Nevis</option>
   <option value="St_Lucia">St Lucia</option>
   <option value="St_Maarten">St Maarten</option>
   <option value="St_Pierre_Miquelon">St Pierre & Miquelon</option>
   <option value="St_Vincent_Grenadines">St Vincent & Grenadines</option>
   <option value="Saipan">Saipan</option>
   <option value="Samoa">Samoa</option>
   <option value="Samoa_American">Samoa American</option>
   <option value="San _Marino">San Marino</option>
   <option value="Sao_Tome_Principe">Sao Tome & Principe</option>
   <option value="Saudi_Arabia">Saudi Arabia</option>
   <option value="Senegal">Senegal</option>
   <option value="Seychelles">Seychelles</option>
   <option value="Sierra_Leone">Sierra Leone</option>
   <option value="Singapore">Singapore</option>
   <option value="Slovakia">Slovakia</option>
   <option value="Slovenia">Slovenia</option>
   <option value="Solomon_Islands">Solomon Islands</option>
   <option value="Somalia">Somalia</option>
   <option value="South_Africa">South Africa</option>
   <option value="Spain">Spain</option>
   <option value="Sri_Lanka">Sri Lanka</option>
   <option value="Sudan">Sudan</option>
   <option value="Suriname">Suriname</option>
   <option value="Swaziland">Swaziland</option>
   <option value="Sweden">Sweden</option>
   <option value="Switzerland">Switzerland</option>
   <option value="Syria">Syria</option>
   <option value="Tahiti">Tahiti</option>
   <option value="Taiwan">Taiwan</option>
   <option value="Tajikistan">Tajikistan</option>
   <option value="Tanzania">Tanzania</option>
   <option value="Thailand">Thailand</option>
   <option value="Togo">Togo</option>
   <option value="Tokelau">Tokelau</option>
   <option value="Tonga">Tonga</option>
   <option value="Trinidad_Tobago">Trinidad & Tobago</option>
   <option value="Tunisia">Tunisia</option>
   <option value="Turkey">Turkey</option>
   <option value="Turkmenistan">Turkmenistan</option>
   <option value="Turks_Caicos_Is">Turks & Caicos Is</option>
   <option value="Tuvalu">Tuvalu</option>
   <option value="Uganda">Uganda</option>
   <option value="United_Kingdom">United Kingdom</option>
   <option value="Ukraine">Ukraine</option>
   <option value="United_Arab_Erimates">United Arab Emirates</option>
   <option value="United_States_of_America">United States of America</option>
   <option value="Uraguay">Uruguay</option>
   <option value="Uzbekistan">Uzbekistan</option>
   <option value="Vanuatu">Vanuatu</option>
   <option value="Vatican_City_State">Vatican City State</option>
   <option value="Venezuela">Venezuela</option>
   <option value="Vietnam">Vietnam</option>
   <option value="Virgin_Islands_(Brit)">Virgin Islands (Brit)</option>
   <option value="Virgin_Islands_(USA)">Virgin Islands (USA)</option>
   <option value="Wake_Island">Wake Island</option>
   <option value="Wallis_Futana_Is">Wallis & Futana Is</option>
   <option value="Yemen">Yemen</option>
   <option value="Zaire">Zaire</option>
   <option value="Zambia">Zambia</option>
   <option value="Zimbabwe">Zimbabwe</option>
                           <option value="other">Other</option>
                        </select>
                     </div>

                     <div class="xs-input-group other_country">
                        <label for="xs-donate-name">Enter Country<span class="color-light-red">*</span></label>
                        <input type="text" value="{{ $memberData->other_country_input }}" name="other_country" class="form-control other_country_input" placeholder="Enter Country">
                     </div>

                     <div class="xs-input-group">
                        <label for="xs-donate-name">Contact Number<span class="color-light-red">*</span></label>
                        <input type="text" value="{{ $memberData->mobile }}" required name="contact_number" class="form-control" placeholder="Your Contact Number">
                     </div>
                     <div class="xs-input-group">
                        <label for="xs-donate-name">Emergency Contact Number<span class="color-light-red">*</span></label>
                        <input type="text" value="{{ $memberData->alternate_mobile }}" required name="emrgency_number" class="form-control" placeholder="Emergency Contact Number">
                     </div>
                     <div class="xs-input-group">
                        <label for="xs-donate-name">Referred by?</label>
                        <input type="text" value="{{ $memberData->reference }}" name="reference" class="form-control" placeholder="Reference Name - If any">
                     </div>




                     <button type="submit" class="btn btn-success btn-sm">Save </button>
                     <input type="hidden" id="csrf-token" value="{{csrf_token()}}" name="_token">
                     <input type="hidden" value="{{ $memberData->user_id }}" name="user_id">
                  </form>
               </div>
            </div>
            <div class="col-lg-6">
               <div class="xs-text-content xs-pr-20">

                  @if( strlen($memberData->college) <= 0 || strlen($memberData->alternate_mobile) <= 0 )
                  <h2 class="color-navy-blue" style="font-size: 26px;">Alert ... ! </h2>
                  @endif
                  @if( strlen($memberData->college) <= 0 )
                  <div class="alert alert-warning">Please update your College or School Name.</div>

                  @endif

                  @if( strlen($memberData->alternate_mobile) <= 0 )
                  <div class="alert alert-warning">Please update your Emergency Contact Number.</div>
                  @endif
                  <h2 class="color-navy-blue" style="font-size: 26px;">Change Password </h2>
                     <span class="xs-separetor v2"></span>

                  <form action="{{ route('passwordProcess') }}" method="POST" >
                     <div class="xs-input-group">
                        <label for="xs-donate-name">New Password<span class="color-light-red">*</span></label>
                        <input type="text" required name="member_password" class="form-control" placeholder="Secret">
                     </div>

                     <button type="submit" disabled="" class="btn disabled btn-success btn-sm">Save </button>
                     <input type="hidden" id="csrf-token" value="{{csrf_token()}}" name="_token">
                     <input type="hidden" value="{{ $memberData->user_id }}" name="user_id">
                  </form>
               </div>
            </div>
         </div>
      </div>
   </section>
</main>

<script type="text/javascript">
   $(document).ready(function(){
    $('.other_country').hide();
   $(".country").change(function() {
  if ($(this).val() == "other") {
    $('.other_country').show();
    $('.other_country_input').attr('required', '');
  } else {
    $('.other_country').hide();
    $('.other_country_input').removeAttr('required');
  }
});
});
</script>
   
@if (strlen($memberData->country) > 0)
<script type="text/javascript">
$(document).ready(function() {
$(".country").val('{{ $memberData->country }}');
});
</script>
@endif


@if (strlen($memberData->country) > 0)
<script type="text/javascript">
$(document).ready(function() {
$('.country').trigger('change');
});
</script>
@endif

@if (strlen($memberData->gender) > 0)
<script type="text/javascript">
$(document).ready(function() {
$(".member_gender").val('{{ $memberData->gender }}');
});
</script>
@endif

@endsection

