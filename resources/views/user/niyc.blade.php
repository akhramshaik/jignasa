@extends('user.master')
@section('content')
@include('user.master_header')

<style type="text/css">
   p {
          text-align: justify;
   }
</style>
<main class="xs-main" style="margin-top: 150px;">
   <section class="xs-content-section-padding">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="xs-event-banner">
                  <img src="assets/images/event/event-banner.jpg" alt="">
               </div>
               <div class="row">
                  <div class="col-lg-12 xs-event-wraper">
                     <div class="xs-event-content">
                        <h4> NIYC - National Integration Youth Camp </h4>
                        <p class="lead">NATIONAL INTEGRATION YOUTH CAMP (NIYC) conducted across the 13 districts of Andhra Pradesh India to inculcate the spirit of togetherness among the youth participants from across the state. Engaging youth in various team building activities for skill development of the youth, Fine Arts, Culture, Heritage among the students by actively engaging them as participants, ambassadors, viewers of the various activities regarding Culture, Fine Arts and Heritage of Andhra Pradesh to create a generation of patrons for Andhra Pradesh Arts, Culture and Heritage with strong enlightenment and awareness.</p>
                     </div>

                     <div class="xs-about-feature xs-mb-30">
                        <h3>Our Vision</h3>
                        <p class="lead" style="padding-bottom: 20px;">NATIONAL INTEGRATION YOUTH CAMP (NIYC) conducted to inculcate the spirit of togetherness among the participants throughout the state, Engaging youth in various team building activities for skill development of the youth, meeting different kinds of people from different cultures experiencing different geographical, historical and societal situations, the students shall get a valuable experience and glimpse of  people.</p>

                                                <p class="lead" style="padding-bottom: 20px;">To revive such wonderful vision, JIGNASA is conducting NATIONAL INTEGRATION YOUTH CAMP (NIYC) to inculcate the spirit of National integration among youth, Fine Arts, Culture, and Heritage among the students by actively engaging them as participants, ambassadors, and viewers of the various activities regarding Culture, Fine Arts and Heritage of Andhra Pradesh. By this initiative of engaging students, we aspire to create a generation of patrons for Andhra Pradesh Arts, Culture and Heritage with strong enlightenment and awareness infused regarding these [Arts, Culture and Heritage] among students of our state.</p>
                     </div>
                     <div class="xs-horizontal-tabs">
                        <ul class="nav nav-tabs" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" href="#facilities" role="tab">Highlights</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#mapLocation" role="tab">Location</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#contactUs" role="tab">Contact us</a>
                           </li>
                        </ul>

                        <div class="tab-content">
                           <div class="tab-pane fade show active" id="facilities" role="tabpanel">
                              <div class="row">
                                 <div class="col-md-12">
                                    <ul class="xs-unorder-list circle green-icon">
                                       <li>Step towards togetherness and National Integration.</li>
                                       <li>Engaging youth in Team Building & Skill Development activities.</li>
                                    </ul>
                                 </div>
                   
                              </div>
                           </div>

                           <div class="tab-pane" id="mapLocation" role="tabpanel">
                              <div id="xs-map"></div>
                           </div>
                           <div class="tab-pane" id="contactUs" role="tabpanel">
                              <div class="xs-contact-form-wraper">
                                 <form action="#" method="POST" id="xs-contact-form" class="xs-contact-form">
                                    <div class="input-group">
                                       <input type="text" name="name" id="xs-name" class="form-control" placeholder="Enter Your Name.....">
                                       <div class="input-group-append">
                                          <div class="input-group-text"><i class="fa fa-user"></i></div>
                                       </div>
                                    </div>
                                    <div class="input-group">
                                       <input type="email" name="email" id="xs-email" class="form-control" placeholder="Enter Your Email.....">
                                       <div class="input-group-append">
                                          <div class="input-group-text"><i class="fa fa-envelope-o"></i></div>
                                       </div>
                                    </div>
                                    <div class="input-group massage-group">
                                       <textarea name="massage" placeholder="Enter Your Message....." id="xs-massage" class="form-control" cols="30" rows="10"></textarea>
                                       <div class="input-group-append">
                                          <div class="input-group-text"><i class="fa fa-pencil"></i></div>
                                       </div>
                                    </div>
                                    <button class="btn btn-success disabled" disabled="" type="submit" id="xs-submit">submit</button>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>

               </div>
            </div>
         </div>
      </div>
   </section>
</main>
@endsection