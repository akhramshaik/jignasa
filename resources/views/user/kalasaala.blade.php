@extends('user.master')
@section('content')
    @include('user.master_header')
    <main class="xs-main" style="margin-top: 150px;">
        <section class="xs-content-section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="xs-event-banner">
                            <img src="assets/images/event/kala.jpeg" alt="">
                        </div>

                        <div class="row">
                            <div class="col-lg-12 xs-event-wraper" style="padding: 15px 0px;">
                                <center>
                                    @if(Sentinel::getUser())
                                        <a href="{{ route('kalasaalaApplication') }}">
                                            <button type="button" class="btn btn-success">Register Now</button>
                                        </a>
                                    @else
                                        <a href="{{ route('login') }}">
                                            <button type="button" class="btn btn-success">Register Now</button>
                                        </a>
                                    @endif
                                </center>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 xs-event-wraper">
                                <div class="xs-event-content">
                                    <h4>About Jignasa Kalasaala</h4>
                                    <p class="lead">A perfect place to learn Life Skills through the medium of Art….</p>
                                    <p class="lead">JIGNASA Kalasaala is initiated to provide education through the
                                        medium of art that focuses on cultivating UNESCO identified core Life Skills
                                        such as Decision Making, Problem Solving, Creative thinking, Critical Thinking,
                                        Effective communication, Interpersonal Relationship skills, Self awareness,
                                        Empathy, Coping with stress and Coping with emotions for adolescents as well as
                                        for children. Through this, we strive to impart qualities like Creativity,
                                        Confidence, Problem Solving, Perseverance, Focus, Nonverbal Communication,
                                        Receiving constructive feedback, Collaboration, Dedication & Accountability to
                                        make them true 21st Century Global citizens.</p>
                                    <p class="lead" style="color: #ff853f;">Our VIRTUAL classroom doors are going to
                                        open in a broader way for</p>
                                    <ul class="xs-unorder-list circle green-icon">
                                        <li>Dance</li>
                                        <li>Music</li>
                                        <li>Dramatics</li>
                                        <li>Arts and Crafts</li>
                                        <li>Literary and Speaking</li>
                                    </ul>
                                    <p class="lead" style="color: #ff853f;margin-top: 40px;">Age Category – 3 Groups</p>
                                    <ul class="xs-unorder-list circle green-icon">
                                        <li>Sub Junior (Age 4 - 10)</li>
                                        <li>Junior (Age 11 to 17)</li>
                                        <li>Senior (Age 17+)</li>
                                    </ul>
                                </div>


                                <div class="xs-about-feature xs-mb-30">
                                    <h3>Why Kalasaala</h3>
                                    <ul class="xs-unorder-list circle green-icon">
                                        <li>International classroom</li>
                                        <li>UN recognized life skills through the medium of art</li>
                                        <li>Activity oriented learning</li>
                                        <li>Thematic workshops – Competitions – Art experience virtual tours</li>
                                        <li>Mentorship from prominent leaders and eminent personalities in the field of
                                            art
                                        </li>
                                        <li>Encouraging learning through simulating real-life circumstances</li>
                                        <li>Creative – Dynamic – Accomplished trainers</li>
                                        <li>Time travel through cultural heritage stories of India</li>
                                        <li>Certifications from internationally recognized organizations in art, culture
                                            and heritage
                                        </li>
                                    </ul>
                                </div>



                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection