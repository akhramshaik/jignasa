@extends('user.master')
@section('content')
@include('user.master_header')
<style type="text/css">
   p {
   text-align: justify;
   }
</style>
<main class="xs-main" style="margin-top: 100px;">
   <section class="xs-content-section-padding">
      <div class="container">
         <div class="row">
            <div class="col-lg-11 content-center">
               <div class="xs-heading xs-mb-100 text-center">
                  <h2 style="font-size: 30px;" class="xs-mb-0 xs-title">About  <span class="color-green">JIGNASA</span> </h2>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-6">
               <div class="xs-about-feature">
                  <h3>Our Mission</h3>
                  <p class="lead">To create and develop business, commerce, education and industry in the areas of Heritage, Culture, Social Responsibility, Fine Arts, Literature, Tourism on par with technology, so that these sectors would create ample employment opportunities catering the needs of youth empowering them and helping them to work as per their nature ,live and pursue their dream careers.</p>
               </div>
            </div>
            <div class="col-md-6">
               <div class="xs-about-feature">
                  <h3>Our Vision</h3>
                  <p class="lead">To create dynamic individuals in the areas of Fine Arts, Heritage, Culture, Literature, Technology for building a dynamic world heading in right direction. “Education is that, which not only includes mere academics, It must include Fine arts, Social Responsibility, Heritage, Culture, Literature, Personality development as a part of it, for the overall growth of an individual”.
                  </p>
               </div>
            </div>
         </div>
      </div>
   </section>
   <div class="xs-funfact-section xs-content-section-padding waypoint-tigger parallax-window" style="background-image: url('assets/images/backgrounds/parallax_1.jpg')">
      <div class="container">
         <div class="row col-lg-10 xs-heading mx-auto">
            <h2 class="xs-title color-white small">Our Journey so far</h2>
         </div>
         <div class="row">
            <div class="col-lg-3 col-md-6">
               <div class="xs-single-funFact color-white">
                  <i class="fa fa-building-o" style="font-size: 40px;"></i>
                  <span class="number-percentage-count number-percentage" data-value="25" data-animation-duration="3500">0</span><span>+</span>
                  <small>Colleges</small>
               </div>
            </div>
            <div class="col-lg-3 col-md-6">
               <div class="xs-single-funFact color-white">
                  <i class="fa fa-users" style="font-size: 40px;"></i>
                  <span class="number-percentage-count number-percentage" data-value="150000" data-animation-duration="3500">0</span><span>+</span>
                  <small>Students</small>
               </div>
            </div>
            <div class="col-lg-3 col-md-6">
               <div class="xs-single-funFact color-white">
                  <i class="fa fa-ticket" style="font-size: 40px;"></i>
                  <span class="number-percentage-count number-percentage" data-value="600" data-animation-duration="3500">0</span><span>+</span>
                  <small>Events</small>
               </div>
            </div>
            <div class="col-lg-3 col-md-6">
               <div class="xs-single-funFact color-white">
                  <i class="fa fa-handshake-o" style="font-size: 40px;"></i>
                  <span class="number-percentage-count number-percentage" data-value="50" data-animation-duration="3500">0</span><span>+</span>
                  <small>Associations</small>
               </div>
            </div>
         </div>
      </div>
      <div class="xs-black-overlay"></div>
   </div>
   <section class="xs-section-padding" style="padding-bottom: 0px;" >
      <div class="container">
         <div class="xs-heading row xs-mb-60">
            <div class="col-md-12 col-xl-12">
               <h2 style="font-size: 30px;" class="xs-title">More About Us</h2>
               <p style="padding-bottom: 20px;">JIGNASA is an organization started with a motto to promote and execute the vision of First Indian Nobel Laurette Sri Ravindranath Tagore who aptly said Education is that which not only include mere academics ,it must include Fine Arts, Culture , Heritage, Social Responsibility as a part of it for over all development of the individual.</p>
               <p style="padding-bottom: 20px;">JIGNASA acts as a medium and interface among UN initiatives, Central and State Government initiatives, Corporate Companies , Educational institutions.</p>
               <p style="padding-bottom: 20px;">We want to promote Entrepreneurship in Culture, Fine Arts, Heritage, Literature through promoting Life Skill Development and Creative Leadership with Education initiatives and activities.</p>
               <p style="padding-bottom: 20px;color: #ff853f;">JIGNASA is the Andhra Pradesh and Telangana States’ Largest Professional Student Network with around 1200+ organisers,  8000+ volunteers , 900+ corporate employees impacting over 1,50,000+ Professional students directly through education initiatives.</p>
               <p style="padding-bottom: 20px;">JIGNASA had initiated, guided and mentored around 200+ Student Activity Clubs in Technology, Literature, Fine Arts, Dance, Drama, Music, Culture & Heritage and Environment & Social Responsibility in 25+ Engineering colleges impacting 1,50,000+ students on par with standards of Student Activity Clubs in IITs and IIMs.</p>
            </div>
         </div>
      </div>
   </section>

   <section class="xs-partner-section" style="padding-top: 0px;padding-bottom: 0px;background-image: url('{{ URL::asset('user/images/map.png') }}');">
      <div class="container">
         <div class="row">
            <div class="col-lg-5">
               <div class="xs-partner-content">
                  <div class="xs-heading xs-mb-40">
                     <h2 style="font-size: 30px;" class="xs-mb-0 xs-title">Trusted by the biggest <span class="color-green">brand.</span></h2>
                  </div>
               <p>To promote Entrepreneurship in Culture, Fine Arts, Heritage, Literature through promoting Life Skill Development and Creative Leadership with Education initiatives and activities. Jignasa has associated with these elite bodies as a part of different initiatives.</p>

                  <a href="#" class="btn btn-primary bg-orange">
                  More...
                  </a>
               </div>
            </div>
            <div class="col-lg-7">
               <ul class="fundpress-partners">
                  <li><a href="#"><img src="{{ URL::asset('user/images/partner/client_11.png') }}" alt=""></a></li>
                  <li><a href="#"><img src="{{ URL::asset('user/images/partner/client_12.png') }}" alt=""></a></li>
                  <li><a href="#"><img src="{{ URL::asset('user/images/partner/client_13.png') }}" alt=""></a></li>
                  <li><a href="#"><img src="{{ URL::asset('user/images/partner/client_14.png') }}" alt=""></a></li>
                  <li><a href="#"><img src="{{ URL::asset('user/images/partner/client_15.png') }}" alt=""></a></li>
               </ul>
            </div>
         </div>
      </div>
   </section>
</main>
@endsection