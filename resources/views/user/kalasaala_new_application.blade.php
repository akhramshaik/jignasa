@extends('user.master')
<style type="text/css">
    .razorpay-payment-button {
        color: #fff;
        float: right;
        background-color: #28a745;
        border-color: #28a745;
        display: inline-block;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        border: 1px solid transparent;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: 0.25rem;
        transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }

    .razorpay-payment-button:hover, .razorpay-payment-button:focus {
        text-decoration: none;
    }

    .razorpay-payment-button:focus, .razorpay-payment-button.focus {
        outline: 0;
        box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
    }
</style>
@section('content')
    @include('user.master_header')
    <main class="xs-main" style="margin-top: 150px;">
        <section class="xs-section-padding" style="padding: 10px 0;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="xs-text-content xs-pr-20">
                            <h2 class="color-navy-blue" style="font-size: 25px;">Kalasaala Application</h2>
                        </div>
                        <div class="col-lg-6" style="margin-left: -14px;">
                            <div class="form-group">
                                <label for="sel1">Age Category</label>
                                <select class="form-control age_cat">
                                    <option value="sub_junior">Sub Junior (Age 4 - 10)</option>
                                    <option value="junior">Junior (Age 11 to 17)</option>
                                    <option value="senior">Senior (Age 17+)</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="sel1">Art Forms</label>
                                <select class="form-control art_cat">
                                    <option value="dance">Dance</option>
                                    <option value="music">Music</option>
                                    <option value="dramatics">Dramatics</option>
                                    <option value="arts_and_crafts">Arts and Crafts</option>
                                    <option value="literary_and_speaking">Literary and Speaking</option>

                                </select>
                            </div>
                        </div>
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td>Your Name</td>
                                <td>{{ $memberInfo->name }}</td>
                            </tr>
                            <tr>
                                <td>Your School or College Name</td>
                                <td>{{ $memberInfo->college }}</td>
                            </tr>
                            <tr>
                                <td>Your Email</td>
                                <td>{{ $memberInfo->email }}</td>
                            </tr>
                            <tr>
                                <td>Your Mobile</td>
                                <td>{{ $memberInfo->mobile }}</td>
                            </tr>
                            @if( $memberInfo->country == 'india')
                                <tr>
                                    <td>Kalasaala Amount</td>
                                    <td>₹800</td>
                                </tr>
                                <tr>
                                    <td>Payment Handling Charges + GST</td>
                                    <td>₹160</td>
                                </tr>
                            @else
                                <tr>
                                    <td>Kalasaala Amount</td>
                                    <td>$40</td>
                                </tr>
                                <tr>
                                    <td>Payment Handling Charges + GST</td>
                                    <td>$8</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-12 col-lg-12">
                        <div class="xs-text-content xs-pr-20">
                            @if( $memberInfo->country == 'india')
                                <form action="{{ route('paymentProcess') }}" method="POST">
                                    <input type="hidden" name="user_id" value="{{ $memberInfo->member_id }}">
                                    <input type="hidden" class="in_art_cat" name="art_cat" value="">
                                    <input type="hidden" class="in_age_cat" name="age_cat" value="">
                                    <input type="hidden" id="csrf-token" value="{{csrf_token()}}" name="_token">
                                    <button type="submit" style="float: right;" class="btn btn-success pay_info">Pay
                                        ₹960
                                    </button>
                                </form>
                            @else
                                <form action="{!!route('paymentProcessRazar')!!}" method="POST">
                                    <input type="hidden" class="in_art_cat" name="art_cat" value="">
                                    <input type="hidden" class="in_age_cat" name="age_cat" value="">

                                    <script src="https://checkout.razorpay.com/v1/checkout.js"
                                            data-key="{{ env('RAZOR_KEY') }}"
                                            data-amount="4800"
                                            data-currency="USD"
                                            data-buttontext="Pay $48"
                                            data-name="Jignasa"
                                            data-notes.akki="aaaa"
                                            data-notes.akki1="ffff"
                                            data-description="Jignasa - Kalasaala Payment"
                                            data-image="https://jignasa.org/assets/images/logo.png"
                                            data-prefill.name="{{$memberInfo->name}}"
                                            data-prefill.email="{{$memberInfo->email}}"
                                            data-theme.color="#ff7529">
                                    </script>
                                    <input type="hidden" name="_token" value="{!!csrf_token()!!}">
                                </form>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-12" style="padding-bottom: 35px;">
                        <div class="xs-text-content xs-pr-20">
                            <h2 class="color-navy-blue" style="font-size: 25px;">Kalasaala Guidelines</h2>
                                    <ul class="xs-unorder-list circle green-icon">
                                        <li>We are providing International Virtual Classroom to all the students. All the students are expected to install the ZOOM application on their devices.</li>
                                        <li>Every class will be conducted in a stipulated time limit thus; participants are expected to stay connected with a strong internet connection.</li>
                                        <li>Every student after admission will be issued a Student ID through WhatsApp number/Email address provided during registration. Depending upon the time zone, Classes will be scheduled according to the Art category you opted to learn.</li>
                                        <li>After admission – The student will be provided with a Unique ID – where Student data/ Student Assessment/ Feedback forms of parents will be linked with it so that your data will be updated online.</li>
                                        <li>Students after admission will receive the Zoom/WhatsApp link through mail & other contact information.</li>
                                        <li>Students can register for one or many courses. Those who have registered for many courses will follow the timings strictly according to the fixed timings allotted to you where communication will be sent you through Email or Whatsapp on your registered Email Id and Contact numbers.</li>
                                        <li>There are common guidelines for all the Art Courses, but all the Course timings will be allotted separately on age-wise Category.</li>
                                        <li>Any dynamic announcements are expected to get published through our official social media pages of “Jignasa.org”, thus all the candidates are expected to follow them for regular updates.</li>
                                        <li>Kindly register your details properly as your entire communication will be sent through your registered Email Id and Contact numbers.</li>
                                        <li>No restrictions on Age, Gender, and Region. Kalasaala is open for all age Categories across the Globe.</li>
                                        <li>Kindly register for a course in your respected age category.</li>
                                        <li>For any communication/queries, please contact +91- 97003 30899 or send a mail to our support team hr@jignasa.com</li>
                                    </ul>
                        </div>
                    </div>




                    <div class="col-md-12 col-lg-12">
                        <div class="xs-text-content xs-pr-20">
                            <h2 class="color-navy-blue" style="font-size: 25px;">Confirmation</h2>
                            <p>Please do validate that your information above is correct. If you have anything to be
                                modified plese <a href="{{ route('myaccount') }}"> Click Here </a> to modify them. All
                                the information collected here is for security reasons only, not for any financial
                                exchange. This information is secured with jignasa and not published in any public
                                domains.</p>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12" style="    margin-top: 20px;">
                        <div class="xs-text-content xs-pr-20">
                            <h2 class="color-navy-blue" style="font-size: 25px;">Need Assistance ?</h2>
                            <p>For any queries and further information email us on info@jignasa.org and contact us at
                                +91 - 97003 30899.</p>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </main>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".pay_info").hover(function () {
                art_cat = $(".art_cat").val();
                age_cat = $(".age_cat").val();
                $(".in_art_cat").val(art_cat);
                $(".in_age_cat").val(age_cat);
            });
            $(".razorpay-payment-button").hover(function () {
                art_cat = $(".art_cat").val();
                age_cat = $(".age_cat").val();
                $(".in_art_cat").val(art_cat);
                $(".in_age_cat").val(age_cat);
            });
        });
    </script>
@endsection