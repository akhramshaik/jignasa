@extends('user.master')
@section('content')
@include('user.master_header')
<main class="xs-main" style="margin-top: 100px;">
   <section class="xs-section-padding ">
      <div class="container">


      </div>
   </section>
   <section class="xs-section-padding" style="padding: 10px 0;">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-lg-12">
               <div class="xs-text-content xs-pr-20">
                  <h2 class="color-navy-blue" style="font-size: 25px;">Jignasa Privacy Policy </h2>
                  <p>Effective date: August 09, 2018</p>
<p>JIGNASA INTERFACE PVT LTD we operates the http://www.jignasa.org website Service.</p>
<p>This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use our Service and the choices you have associated with that data. </p>
<p>We use your data to provide and improve the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, accessible from http://www.jignasa.org</p>
<p></p>
<p></p>

               </div>
            </div>
<div class="col-md-12 col-lg-12" style="    margin-top: 50px;">
<div class="xs-text-content xs-pr-20">
<h2 class="color-navy-blue" style="font-size: 25px;">Information Collection And Use</h2>                
<p>We collect mostly individual Name, Email and Mobile Number for various communication purposes to provide and improve our Service to you.</p>
</div>
</div>


<div class="col-md-12 col-lg-12" style="    margin-top: 50px;">
<div class="xs-text-content xs-pr-20">
<h2 class="color-navy-blue" style="font-size: 25px;">Transfer Of Data</h2>                
<p>Your information, including Personal Data, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.</p>
<p>If you are located outside India and choose to provide information to us, please note that we transfer the data, including Personal Data, to India and process it there.</p>
<p>Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>
<p>JIGNASA INTERFACE PVT LTD will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.</p>
</div>
</div>




            <div class="col-md-12 col-lg-12" style="    margin-top: 50px;">
               <div class="xs-text-content xs-pr-20">
                  <h2 class="color-navy-blue" style="font-size: 25px;">Need Assistance ?</h2>
                  <p>For any queries and further information email us on info@jignasa.org and contact us at +91 - 94916 89304.</p>
               </div>
            </div>
         </div>
      </div>
   </section>
</main>
@include('user.master_footer')
@endsection