@extends('user.master')
@section('content')
@include('user.master_header')
<main class="xs-main" style="margin-top: 200px;">
   <section class="xs-content-section-padding">
      <div class="container">
         <div class="row">

            @foreach($paymentDetails as $details)
                     <?php 
                        $paymentDetailsFull = json_decode($details->data);
                     ?>

            <div class="col-lg-6 row xs-single-event event-green">
               <div class="col-md-5">
                  <div class="xs-event-image">
                     <img src="{{ url('/') }}/assets/event_sample.jpeg" alt="">
                     <div class="xs-entry-date">
                        <span class="entry-date-day">{{ date('d', strtotime($details->created_at)) }}</span>
                        <span class="entry-date-month">{{ date('M', strtotime($details->created_at)) }} - {{ date('Y', strtotime($details->created_at)) }}</span>
                     </div>
                     <div class="xs-black-overlay"></div>
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="xs-event-content">
                     <a href="#">{{ $paymentDetailsFull->productinfo  }}</a>
                     <p style="padding-bottom: 0px;margin-bottom: 0px;">Rs. {{ $paymentDetailsFull->amount  }} </p>
                     <p style="padding-bottom: 0px;margin-bottom: 0px;">Transaction ID: {{ $paymentDetailsFull->txnid  }} </p>
                     <a href="#" class="btn btn-primary">
                     View Ticket
                     </a>
                  </div>
               </div>
            </div>
            @endforeach

         </div>
      </div>
   </section>
</main>
@endsection