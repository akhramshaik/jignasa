<header class="xs-header header-transparent xs-box">
   <div class="container">
      <nav class="xs-menus">
         <div class="xs-top-bar clearfix">
            <ul class="xs-top-social">
               <li><a href="https://www.facebook.com/JIGNASA.ORG/" target="_blank" ><i class="fa fa-facebook"></i></a></li>
               <li><a href="https://twitter.com/jignasa_org" target="_blank"><i class="fa fa-twitter"></i></a></li>
               <li><a href="https://www.instagram.com/jignasaorg/" target="_blank"><i class="fa fa-instagram"></i></a></li>
            </ul>
            <a href="mailto:info@jignasa.org" class="xs-top-bar-mail"><i class="fa fa-envelope-o"></i>info@jignasa.org</a>
            <a href="mailto:info@jignasa.org" class="xs-top-bar-mail"><i class="fa fa-phone-square"></i>+91 - 97003 30899</a>
         </div>
         <div class="nav-header">
            <div class="nav-toggle"></div>
            <a href="index.html" class="xs-nav-logo">
            <img src="assets/images/logo.png" alt="">
            </a>
         </div>
         <div class="nav-menus-wrapper row">
            <div class="xs-logo-wraper col-lg-2 col-xl-2 xs-padding-0">
               <a class="nav-brand" href="{{ route('home') }}">
               <img src="assets/images/logo.png" alt="">
               </a>
            </div>
            <div class="col-lg-10 col-xl-7">
               <ul class="nav-menu">
                  <li><a href="{{ route('home') }}">Home</a></li>
                  <li><a href="{{ route('about') }}">About Us</a></li>
                  <li>
                     <a href="#">Our Initiatives</a>
                     <ul class="nav-dropdown">
                        <li><a href="{{ route('sac') }}">SAC - Student Activity Centre</a></li>
                        <li><a href="{{ route('kalasaala') }}">Kalasaala</a></li>
                        <li><a href="{{ route('bhuvana-vijayam') }}">Bhuvana Vijayam</a></li>
                        <!-- <li><a href="{{ route('jignasayaan') }}">WAK WACS - Weekend Activity Centers</a></li> -->
                        <li><a href="{{ route('jignasayaan') }}">Jignasayaan</a></li>
                        <!-- <li><a href="{{ route('artathon') }}">Artathon</a></li> -->
                        <!-- <li><a href="{{ route('amaravatiChandrudu') }}">Amaravati Chandrudu</a></li> -->
                        <!-- <li><a href="{{ route('nIYC') }}">NIYC </a></li> -->
                        <li><a href="{{ route('mockParliament') }}">Mock Parliament  </a></li>
                        <!-- <li><a href="{{ route('uniFest') }}">Unifest</a></li> -->
                        <!-- <li><a href="{{ route('ekBharatShresthaBharat') }}">Ek Bharat Shrestha Bharat</a></li> -->
                     </ul>
                  </li>
                  <!-- <li><a href="{{ route('events') }}">Events</a></li> -->
                  <li><a href="{{ route('collaborations') }}">collaborations</a></li>
                  <li><a href="{{ route('sponsers') }}">sponsors</a></li>
                  <li><a href="{{ route('contact') }}">Contact</a></li>
               </ul>
            </div>



            @if(Sentinel::getUser())
            <div class="xs-navs-button d-flex-center-end col-lg-3 col-xl-3 d-block d-lg-none d-xl-block" style="padding: 0px;">
               <ul class="nav-menu" style="text-align: center;">
                  <li>
                     <a href="#">Hi {{ Sentinel::getUser()->first_name }},</a>
                     <ul class="nav-dropdown">
                        @if (Sentinel::getUser()->email =='admin@jignasa.org')
                        <li><a href="{{ route('registrationsList') }}">Registrations List</a></li>
                        @endif
                        <li><a href="{{ route('myaccount') }}">My Account</a></li>
                        <li><a href="{{ route('kalasaalaApplication') }}">Kalasaala Applications</a></li>
                        <li><a href="{{ route('logout') }}">Logout</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
            @else
            <div class="xs-navs-button d-flex-center-end col-lg-3 col-xl-3 d-block d-lg-none d-xl-block">
               <a href="{{ route('login') }}" class="btn btn-success">
               <span class="badge"><i class="fa fa-user-circle-o"></i></span> Login 
               </a>
            </div>
            @endif
         </div>
      </nav>
   </div>
</header>