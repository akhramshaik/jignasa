@extends('user.master')
@section('content')
@include('user.master_header')
<main class="xs-main" style="margin-top: 320px;">
   <section class="xs-contact-section-v2"  style="margin-bottom: 50px;">
         <div class="container">
            <div class="row">
               <div class="col-md-6 col-lg-8">
                  <div class="xs-contact-details">

                     <ul class="xs-unorder-list">
                        <li><i class="fa fa-phone color-green"></i>+91 - 97003 30899</li>
                        <li><i class="fa fa-envelope-o color-green"></i>hr@jignasa.org</li>
                        <li><i class="fa fa-map-marker color-green"></i> Behind Brew Buzz, Gayatri Nagar, Benz Circle, Vijayawada, Andhra Pradesh - 520008.</li>
                     </ul>
                  </div>

               </div>
            </div>
         </div>
   </section>
</main>
@endsection