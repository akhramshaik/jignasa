@extends('user.master')
@section('content')
@include('user.master_header')
<main class="xs-main" style="margin-top: 150px;">
   <section class="xs-content-section-padding">
      <div class="container">
         <div class="row xs-mb-50">
            <div class="col-lg-4 col-md-6">
               <div class="xs-box-shadow xs-single-journal xs-mb-30">
                  <div class="entry-thumbnail ">
                     <img src="assets/images/blog/blog_1.jpg" alt="">
                  </div>
                  <div class="entry-header">
                     <div class="entry-meta">
                        <span class="date">
                        <a href=""  rel="bookmark" class="entry-date">
                        27th August 2017
                        </a>
                        </span>
                     </div>
                     <h4 class="entry-title">
                        <a href="#">Brilliant After All, A New Album by Rebecca: Help poor people</a>
                     </h4>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 col-md-6">
               <div class="xs-box-shadow xs-single-journal  xs-mb-30">
                  <div class="entry-thumbnail ">
                     <img src="assets/images/blog/blog_2.jpg" alt="">
                  </div>
                  <div class="entry-header">
                     <div class="entry-meta">
                        <span class="date">
                        <a href=""  rel="bookmark" class="entry-date">
                        02 May 2017
                        </a>
                        </span>
                     </div>
                     <h4 class="entry-title">
                        <a href="#">South african pre primary school build for children</a>
                     </h4>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 col-md-6">
               <div class="xs-box-shadow xs-single-journal  xs-mb-30">
                  <div class="entry-thumbnail ">
                     <img src="assets/images/blog/blog_3.jpg" alt="">
                  </div>
                  <div class="entry-header">
                     <div class="entry-meta">
                        <span class="date">
                        <a href=""  rel="bookmark" class="entry-date">
                        13 January 2017
                        </a>
                        </span>
                     </div>
                     <h4 class="entry-title">
                        <a href="#">Provide pure water for syrian poor people</a>
                     </h4>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 col-md-6">
               <div class="xs-box-shadow xs-single-journal xs-mb-30">
                  <div class="entry-thumbnail ">
                     <img src="assets/images/blog/blog_1.jpg" alt="">
                  </div>
                  <div class="entry-header">
                     <div class="entry-meta">
                        <span class="date">
                        <a href=""  rel="bookmark" class="entry-date">
                        27th August 2017
                        </a>
                        </span>
                     </div>
                     <h4 class="entry-title">
                        <a href="#">Brilliant After All, A New Album by Rebecca: Help poor people</a>
                     </h4>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 col-md-6">
               <div class="xs-box-shadow xs-single-journal  xs-mb-30">
                  <div class="entry-thumbnail ">
                     <img src="assets/images/blog/blog_2.jpg" alt="">
                  </div>
                  <div class="entry-header">
                     <div class="entry-meta">
                        <span class="date">
                        <a href=""  rel="bookmark" class="entry-date">
                        02 May 2017
                        </a>
                        </span>
                     </div>
                     <h4 class="entry-title">
                        <a href="#">South african pre primary school build for children</a>
                     </h4>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 col-md-6">
               <div class="xs-box-shadow xs-single-journal  xs-mb-30">
                  <div class="entry-thumbnail ">
                     <img src="assets/images/blog/blog_3.jpg" alt="">
                  </div>
                  <div class="entry-header">
                     <div class="entry-meta">
                        <span class="date">
                        <a href=""  rel="bookmark" class="entry-date">
                        13 January 2017
                        </a>
                        </span>
                     </div>
                     <h4 class="entry-title">
                        <a href="#">Provide pure water for syrian poor people</a>
                     </h4>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 col-md-6">
               <div class="xs-box-shadow xs-single-journal xs-mb-30">
                  <div class="entry-thumbnail ">
                     <img src="assets/images/blog/blog_1.jpg" alt="">
                  </div>
                  <div class="entry-header">
                     <div class="entry-meta">
                        <span class="date">
                        <a href=""  rel="bookmark" class="entry-date">
                        27th August 2017
                        </a>
                        </span>
                     </div>
                     <h4 class="entry-title">
                        <a href="#">Brilliant After All, A New Album by Rebecca: Help poor people</a>
                     </h4>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 col-md-6">
               <div class="xs-box-shadow xs-single-journal  xs-mb-30">
                  <div class="entry-thumbnail ">
                     <img src="assets/images/blog/blog_2.jpg" alt="">
                  </div>
                  <div class="entry-header">
                     <div class="entry-meta">
                        <span class="date">
                        <a href=""  rel="bookmark" class="entry-date">
                        02 May 2017
                        </a>
                        </span>
                     </div>
                     <h4 class="entry-title">
                        <a href="#">South african pre primary school build for children</a>
                     </h4>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 col-md-6">
               <div class="xs-box-shadow xs-single-journal  xs-mb-30">
                  <div class="entry-thumbnail ">
                     <img src="assets/images/blog/blog_3.jpg" alt="">
                  </div>
                  <div class="entry-header">
                     <div class="entry-meta">
                        <span class="date">
                        <a href=""  rel="bookmark" class="entry-date">
                        13 January 2017
                        </a>
                        </span>
                     </div>
                     <h4 class="entry-title">
                        <a href="#">Provide pure water for syrian poor people</a>
                     </h4>
                  </div>
               </div>
            </div>
         </div>
         <ul class="pagination justify-content-center xs-pagination">
            <li class="page-item disabled">
               <a class="page-link" href="#" aria-label="Previous">
               <i class="fa fa-angle-left"></i>
               </a>
            </li>
            <li class="page-item"><a class="page-link active" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item disabled"><a class="page-link" href="#">...</a></li>
            <li class="page-item"><a class="page-link" href="#">12</a></li>
            <li class="page-item">
               <a class="page-link" href="#" aria-label="Next">
               <i class="fa fa-angle-right"></i>
               </a>
            </li>
         </ul>
      </div>
   </section>
</main>
@endsection