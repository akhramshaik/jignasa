@extends('user.master')
@section('content')
@include('user.master_header')

<style type="text/css">
   p {
          text-align: justify;
   }
</style>
<main class="xs-main" style="margin-top: 150px;">
   <section class="xs-content-section-padding">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="xs-event-banner">
                  <img src="assets/images/event/event-banner.jpg" alt="">
               </div>
               <div class="row">
                  <div class="col-lg-12 xs-event-wraper">
                     <div class="xs-event-content">
                        <h4>Mock Parliament </h4>
                        <p class="lead">The Mock Parliament is a unique platform created to foster dialogue and generate partnerships between exceptional youth from private sector, civil society and government. It acts as bridge to fill the gap between government and society. Many youth leaders and social activists will join Mock Parliament to facilitate youth with adequate opportunity to develop leadership skills and direct them to benefit the humanity.</p>
                        <p class="lead">In recent time we have seen & experienced that the youth is eager to see the change and want to see the country grow.  As we have diverse youth and every group is working in silos. This made us realize that to make our nation grow a consolidated approach towards one direction is required.  Hence, we are introducing a platform where experts of each field, whether it’s Politics, Art or business, would be there along with our youth.  Experts will get a chance to show the vision behind each policy and provide small goals to the youth to work towards that direction.  Similarly, youth will get a chance to share their ground level difficulties and requirements so that the experts will consider those and can work towards it.</p>
                     </div>


                     <div class="xs-about-feature xs-mb-30">
                        <h3>Our Vision</h3>
                        <p class="lead" style="color: #ff853f;padding-bottom: 20px;">An active, united & matured youth who is successful and understands the moral and social responsibility.</p>
                        <p class="lead" style="padding-bottom: 20px;">To encourage the speaking, problem analyzing and policy making skills in youth. Which develop interaction between different groups of people and their problems. To perceive the agenda of the session.</p>
                     </div>
                     <div class="xs-horizontal-tabs">
                        <ul class="nav nav-tabs" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" href="#facilities" role="tab">Highlights</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#mapLocation" role="tab">Location</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#contactUs" role="tab">Contact us</a>
                           </li>
                        </ul>

                        <div class="tab-content">
                           <div class="tab-pane fade show active" id="facilities" role="tabpanel">

                              <div class="row">
                                 <div class="col-md-12">
                                    <ul class="xs-unorder-list circle green-icon">
                                       <li>To discuss local, national and international issue.</li>
                                       <li>To know the views of youngsters and try to represent all kinds of interests related to a problem.</li>
                                       <li>To make suitable laws.</li>
                                    </ul>
                                 </div>
                
                              </div>
                           </div>

                           <div class="tab-pane" id="mapLocation" role="tabpanel">
                              <div id="xs-map"></div>
                           </div>
                           <div class="tab-pane" id="contactUs" role="tabpanel">
                              <div class="xs-contact-form-wraper">
                                 <form action="#" method="POST" id="xs-contact-form" class="xs-contact-form">
                                    <div class="input-group">
                                       <input type="text" name="name" id="xs-name" class="form-control" placeholder="Enter Your Name.....">
                                       <div class="input-group-append">
                                          <div class="input-group-text"><i class="fa fa-user"></i></div>
                                       </div>
                                    </div>
                                    <div class="input-group">
                                       <input type="email" name="email" id="xs-email" class="form-control" placeholder="Enter Your Email.....">
                                       <div class="input-group-append">
                                          <div class="input-group-text"><i class="fa fa-envelope-o"></i></div>
                                       </div>
                                    </div>
                                    <div class="input-group massage-group">
                                       <textarea name="massage" placeholder="Enter Your Message....." id="xs-massage" class="form-control" cols="30" rows="10"></textarea>
                                       <div class="input-group-append">
                                          <div class="input-group-text"><i class="fa fa-pencil"></i></div>
                                       </div>
                                    </div>
                                    <button class="btn btn-success disabled" disabled="" type="submit" id="xs-submit">submit</button>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>

               </div>
            </div>
         </div>
      </div>
   </section>
</main>
@endsection