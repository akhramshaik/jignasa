@extends('user.master')
@section('content')
@include('user.master_header')

<section class="xs-welcome-slider">
   <div class="xs-banner-slider owl-carousel">
      <div class="xs-welcome-content" style="background-image: url(assets/images/slider/slider_1.jpg);">
         <div class="container">
            <div class="xs-welcome-wraper color-white">
               <h2>KALASAALA</h2>
               <p>Taking Art to Every HeART</p>
               <div class="xs-btn-wraper">
                  @if(Sentinel::getUser())
                  <a href="{{ route('kalasaalaApplication') }}"><button type="button" class="btn btn-primary"> <span class="badge"><i class="fa fa-calendar"></i></span> Register Now</button></a>
                  @else
                  <a href="{{ route('login') }}"><button type="button" class="btn btn-primary"> <span class="badge"><i class="fa fa-calendar"></i></span> Register Now</button></a>
                  @endif
               </div>
            </div>
         </div>
         <div class="xs-black-overlay"></div>
      </div>
      <div class="xs-welcome-content" style="background-image: url(assets/images/slider/slider_2.jpg);">
         <div class="container">
            <div class="xs-welcome-wraper color-white">
               <h2>Jignasa</h2>
               <p>Revolutionary heads, United for Purpose Promoting Arts Culture and Heritage.</p>
 
            </div>
         </div>
         <div class="xs-black-overlay"></div>
      </div>
      <div class="xs-welcome-content" style="background-image: url(assets/images/slider/slider_3.jpg);">
         <div class="container">
            <div class="xs-welcome-wraper color-white">
               <h2>Jignasa</h2>
               <p>Bringing Arts and Culture to Life.</p>

            </div>
         </div>
         <div class="xs-black-overlay"></div>
      </div>
   </div>
</section>


<section class="waypoint-tigger xs-section-padding">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-lg-6">
            <div class="xs-archive-content">
               <div class="xs-heading">
                  <h2 class="xs-line-title">Our Mission</h2>
                  <h3 class="xs-title big">Jignasa</h3>
               </div>
               <p style="text-align: justify">To create and develop business, commerce, education and industry in the areas of Heritage, Culture, Social Responsibility, Fine Arts, Literature, Tourism on par with technology, so that these sectors would create ample employment opportunities catering the needs of youth empowering them and helping them to work as per their nature ,live and pursue their dream careers.</p>
               <h5>Major Highlights of Jignasa:</h5>
               <ul class="xs-unorder-list arrow">
                  <li>Entertaining Education</li>
                  <li>Creative Leadership</li>
                  <li>Arts Culture & Heritage Entrepreneur Revolution</li>
               </ul>
               <ul class="xs-funfact-list">
                  <li>
                     <p style="font-size: 10px;">
                        <span class="number-percentage-count number-percentage" data-value="25" data-animation-duration="3500">0</span><sup> + </sup>
                     </p>
                     <span>Colleges</span>
                  </li>
                  <li>
                     <p style="font-size: 10px;">
                        <span class="number-percentage-count number-percentage" data-value="150000" data-animation-duration="3500">0</span><sup> + </sup>
                     </p>
                     <span>Students</span>
                  </li>
                  <li>
                     <p style="font-size: 10px;">
                        <span class="number-percentage-count number-percentage" data-value="600" data-animation-duration="3500">0</span><sup> + </sup>
                     </p>
                     <span>Events</span>
                  </li>
                  <li>
                     <p style="font-size: 10px;">
                        <span class="number-percentage-count number-percentage" data-value="50" data-animation-duration="3500">0</span><sup> + </sup>
                     </p>
                     <span>Associations</span>
                  </li>
                  <div class="clearfix"></div>
               </ul>
            </div>
         </div>
         <div class="col-md-12 col-lg-6 row xs-archive-image">
            <div class="col-md-12 xs-mb-30">
               <!-- <img src="assets/images/archive-img-1.jpg" alt="" class="rounded"> -->
               <img src="assets/images/kalasala.jpeg" alt="" class="rounded">

            </div>
            <div class="col-md-6 col-sm-6">
               <img src="assets/images/archive-img-2.jpg" alt="" class="rounded">
            </div>
            <div class="col-md-6 col-sm-6">
               <img src="assets/images/archive-img-3.jpg" alt="" class="rounded">
            </div>
         </div>
      </div>
   </div>
</section>

<section class="bg-navy-blue">
   <div class="container-fulid">
      <div class="xs-feature-content">
         <h2 class="color-white">Inquire Initiate Inspire...<span> Intellect </span></h2>
      </div>
   </div>
</section>

<section class="xs-section-padding">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-lg-6">
            <div class="xs-text-content xs-pr-20">
               <h2 class="color-navy-blue">Jignasa Yaan 2019</h2>
               <p>JIGNASA YAAN is a Knowledge Journey by professional students of local colleges from states of Andhra Pradesh, Telangana, Tamil Nadu to IIT and IIM Campus Festivals helping the students to experience the Knowledge and Creativity through Cross Culture student interaction with students and student organizers of IITs, IIM’s, NIT’s, BITS from different states and corners of our country.</p>
                <h5>Major Highlights of Jignasa Yaan 2019:</h5>
               <ul class="xs-unorder-list arrow" style="margin-bottom: 20px">
                  <li>Cross Culture Student Interaction.</li>
                  <li>Largest professional Student Education On Wheels.</li>
                  <li>Experience broad world domains.</li>
               </ul>

            </div>
         </div>
         <div class="col-md-6 col-lg-3">
            <div class="xs-feature-image">
               <img src="assets/images/features_1.jpg" alt="">
            </div>
         </div>
         <div class="col-md-6 col-lg-3">
            <div class="xs-feature-image">
               <img src="assets/images/features_2.jpg" alt="">
            </div>
         </div>
      </div>
   </div>
</section>

<section class="parallax-window xs-become-a-volunteer xs-section-padding" style="background-image: url('assets/images/backgrounds/volunteer-bg.jpg')">
   <div class="container">
      <div class="row">
         <div class="col-md-8 col-lg-7">
            <div class="xs-volunteer-form-wraper volunteer-version-3">
               <i class="icon-support icon-watermark"></i>
               <h2>Become a Member</h2>
               <p>Come and join us be a part of tribe get latest updates and upcoming event details.</p>
                  <a href="{{ route('register') }}"><button type="submit" class="btn btn-primary bg-green">Register</button></a>
            </div>
         </div>
      </div>
   </div>
</section>

<section class="bg-gray xs-partner-section" style="background-image: url('assets/images/map.png');">
   <div class="container">
      <div class="row">
         <div class="col-lg-5">
            <div class="xs-partner-content">
               <div class="xs-heading xs-mb-40">
                  <h2 class="xs-mb-0 xs-title">Trusted by the biggest <span class="color-green">brand.</span></h2>
               </div>
               <p>To promote Entrepreneurship in Culture, Fine Arts, Heritage, Literature through promoting Life Skill Development and Creative Leadership with Education initiatives and activities. Jignasa has associated with these elite bodies as a part of different initiatives.</p>
               <a href="#" class="btn btn-primary bg-orange">
               join us now
               </a>
            </div>
         </div>
         <div class="col-lg-7">
            <ul class="fundpress-partners">
               <li><a href="#"><img src="assets/images/partner/client_11.png" alt=""></a></li>
               <li><a href="#"><img src="assets/images/partner/client_12.png" alt=""></a></li>
               <li><a href="#"><img src="assets/images/partner/client_13.png" alt=""></a></li>
               <li><a href="#"><img src="assets/images/partner/client_14.png" alt=""></a></li>
               <li><a href="#"><img src="assets/images/partner/client_15.png" alt=""></a></li>
            </ul>
         </div>
      </div>
   </div>
</section>
@endsection