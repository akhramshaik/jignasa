@extends('user.master')
@section('content')
@include('user.master_header')
<main class="xs-main" style="margin-top: 120px;">
   <section class="xs-section-padding ">
      <div class="container">
         @if(Session::has('chk_msg'))
         <div class="alert {{ Session::get('alert-class') }}"> {{ Session::get('chk_msg') }} </div>
         @endif
            <center>
              <span>
              <i style="padding-bottom: 15px;font-size: 90px;color: #32984a;" class="fa fa-smile-o fa-5x" aria-hidden="true"></i>
              </span> 
            </center>
         <div class="alert alert-success">
            <center>
            <strong>Congratulations!</strong> Your Payment was succesfull and your transaction number is {{ $paymentInfo->txnid }}.
            </center>
         </div>
         <div class="row">
            <table class="table table-bordered">
               <tbody>
                  <tr>
                     <td>Registered Name</td>
                     <td>{{ $paymentInfo->firstname }}</td>
                  </tr>
                  <tr>
                     <td>Registered Email</td>
                     <td>{{ $paymentInfo->email }}</td>
                  </tr>    
                  <tr>
                     <td>Registered Mobile</td>
                     <td>{{ $paymentInfo->phone }}</td>
                  </tr>
               </tbody>
            </table>
         </div>
               <p> <a href="{{ route('kalasaalaApplication') }}"> Click Here</a> for My Applications.</p>

      </div>


   </section>


   <section class="xs-section-padding" style="padding: 10px 0;">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-lg-12">
            <div class="xs-text-content xs-pr-20">
               <h2 class="color-navy-blue" style="font-size: 25px;">Need Assistance ?</h2>
               <p>For any queries and further information email us on info@jignasa.org and contact us at +91 - 97003 30899.</p>
            </div>
         </div>

      </div>
   </div>
</section>
</main>

@endsection