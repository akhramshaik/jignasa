@extends('user.master')
@section('content')
@include('user.master_header')

<style type="text/css">
   p {
          text-align: justify;
   }
</style>
<main class="xs-main" style="margin-top: 150px;">
   <section class="xs-content-section-padding">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="xs-event-banner">
                  <img src="assets/images/event/event-banner.jpg" alt="">
               </div>
               <div class="row">
                  <div class="col-lg-12 xs-event-wraper">
                     <div class="xs-event-content">
                        <h4>UNIFEST</h4>
                        <p class="lead">UNIFEST, Andhra Pradesh largest college cultural festival, It’s a melting pot of Indian and global culture, a launch pad for upcoming artists, a showcase of spectacle - where the streets come alive and every step you take is a step towards new experience, where you can never see it all, but what you do see lasts a lifetime, where friendships are born and memories are made into the soft glow of the bonfires. It’s a little bubble in space and time of infinite possibility, in short: the best four days of your life.</p>
                     </div>

                     <div class="xs-about-feature xs-mb-30">
                        <h3>Our Vision</h3>
                        <p class="lead" style="padding-bottom: 20px;">Our Vision is to herald the start of unique cultural happenings in India and to give a chance to young guns to break new ground on the cultural scene. </p>
                     </div>
                     <div class="xs-horizontal-tabs">
                        <ul class="nav nav-tabs" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" href="#facilities" role="tab">Highlights</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#mapLocation" role="tab">Location</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#contactUs" role="tab">Contact us</a>
                           </li>
                        </ul>

                        <div class="tab-content">
                           <div class="tab-pane fade show active" id="facilities" role="tabpanel">

                              <div class="row">
                                 <div class="col-md-12">
                                    <ul class="xs-unorder-list circle green-icon">
                                       <li>Platform for Unique & Upcoming Artists.</li>
                                       <li>Step towards new experiences.</li>
                                    </ul>
                                 </div>
                 
                              </div>
                           </div>

                           <div class="tab-pane" id="mapLocation" role="tabpanel">
                              <div id="xs-map"></div>
                           </div>
                           <div class="tab-pane" id="contactUs" role="tabpanel">
                              <div class="xs-contact-form-wraper">
                                 <form action="#" method="POST" id="xs-contact-form" class="xs-contact-form">
                                    <div class="input-group">
                                       <input type="text" name="name" id="xs-name" class="form-control" placeholder="Enter Your Name.....">
                                       <div class="input-group-append">
                                          <div class="input-group-text"><i class="fa fa-user"></i></div>
                                       </div>
                                    </div>
                                    <div class="input-group">
                                       <input type="email" name="email" id="xs-email" class="form-control" placeholder="Enter Your Email.....">
                                       <div class="input-group-append">
                                          <div class="input-group-text"><i class="fa fa-envelope-o"></i></div>
                                       </div>
                                    </div>
                                    <div class="input-group massage-group">
                                       <textarea name="massage" placeholder="Enter Your Message....." id="xs-massage" class="form-control" cols="30" rows="10"></textarea>
                                       <div class="input-group-append">
                                          <div class="input-group-text"><i class="fa fa-pencil"></i></div>
                                       </div>
                                    </div>
                                    <button class="btn btn-success disabled" disabled="" type="submit" id="xs-submit">submit</button>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>

               </div>
            </div>
         </div>
      </div>
   </section>
</main>
@endsection