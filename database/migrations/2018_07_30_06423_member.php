<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Member extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_details', function (Blueprint $table) {
            $table->increments('member_id');
            $table->string('name');
            $table->string('user_id');
            $table->string('email');
            $table->string('gender');
            $table->string('college')->nullable();
            $table->string('branch')->nullable();
            $table->string('academicyear')->nullable();
            $table->string('year')->nullable();
            $table->string('mobile')->nullable();
            $table->string('alternate_mobile')->nullable();
            $table->string('current_location')->nullable();
            $table->string('country')->nullable();
            $table->string('dob_user')->nullable();
            $table->string('reference')->nullable();
            $table->string('other_country_input')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
